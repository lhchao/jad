package com.jad.web.listener;

import javax.servlet.ServletContext;

import org.springframework.web.context.WebApplicationContext;

import com.jad.rpc.service.DubboContextListener;
import com.jad.web.cms.util.CmsHelper;
import com.jad.web.sys.util.DictHelper;
import com.jad.web.sys.util.UserHelper;

public class JadContextListener extends DubboContextListener{
	
	public WebApplicationContext initWebApplicationContext(ServletContext servletContext) {

//		if (!SystemUtils.printKeyLoadMessage()){ 
//			return null;
//		}
		
//		FormAuthenticationFilter f=new FormAuthenticationFilter();	
		
		WebApplicationContext wc=super.initWebApplicationContext(servletContext);
		
//		CacheClientManager jadCacheManager = SpringContextHolder.getBean("baseCacheManager");
//		jadCacheManager.start(true);
		
		DictHelper.init();
		CmsHelper.init();
		UserHelper.init();
		
		return wc;
	}
	
}
