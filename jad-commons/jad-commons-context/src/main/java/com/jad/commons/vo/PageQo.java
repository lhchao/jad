package com.jad.commons.vo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.context.Global;

public class PageQo implements QueryObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@CURD(label="页码")
	private Integer pageNo;
	
	@CURD(label="每页记录数")
	private Integer pageSize;
	
	@CURD(label="orderBy")
	private String orderBy;
	
	public PageQo(Integer pageNo,Integer pageSize){
		this.pageNo = pageNo;
		this.pageSize = pageSize;
	}
	
	public PageQo(){
		this.pageNo = 1;
		this.pageSize = Integer.valueOf(Global.getConfig("page.pageSize"));
	}
	
	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}


}
