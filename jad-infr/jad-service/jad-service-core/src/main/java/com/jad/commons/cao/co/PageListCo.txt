package com.jad.commons.cao.co;

import java.io.Serializable;

import com.jad.commons.vo.JadBaseVo;

public class PageListCo<VO extends JadBaseVo<ID>,ID extends Serializable> extends VoListCo<VO,ID> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	protected Integer pageNo; // 当前页码
	
	protected Long count; //总记录数
	
	protected Integer pageSize ; //每页显示条数

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}


	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}


	

}
