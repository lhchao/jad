package com.jad.commons.qo;

import java.io.Serializable;

public class UserDataQo<ID extends Serializable> extends DataQo<ID>{
	
	private static final long serialVersionUID = 1L;


	protected String createBy;	// 创建者
	
	protected String updateBy;	// 更新者
	
	
	public UserDataQo(){
		
	}
	public UserDataQo(ID id){
		super(id);
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	
	

}
