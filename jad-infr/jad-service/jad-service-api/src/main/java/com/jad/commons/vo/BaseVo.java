package com.jad.commons.vo;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.InsertConf;
import com.jad.commons.annotation.UpdateConf;

public class BaseVo<ID extends Serializable> implements ValueObject {

	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@CURD(label = "id", showInList = true, showInView = false, required = true, 
			insertConf = @InsertConf(false), updateConf = @UpdateConf(true))
	protected ID id;//

	@CURD(label = "删除标记", showInList = true, showInView = false, required = true, insertConf = @InsertConf(false), updateConf = @UpdateConf(false))
	protected String delFlag; // 删除标记（0：正常；1：删除；2：审核）

	@CURD(label = "创建日期", showInList = true, showInView = false, required = true, insertConf = @InsertConf(false), updateConf = @UpdateConf(false))
	protected Date createDate; // 创建日期

	@CURD(label = "更新日期", showInList = true, showInView = false, required = true, insertConf = @InsertConf(false), updateConf = @UpdateConf(false))
	protected Date updateDate; // 更新日期

	@CURD(label = "创建者", showInList = true, showInView = false, required = true, insertConf = @InsertConf(false), updateConf = @UpdateConf(false))
	protected String createBy; // 创建者

	@CURD(label = "更新者", showInList = true, showInView = false, required = true, insertConf = @InsertConf(false), updateConf = @UpdateConf(false))
	protected String updateBy; // 更新者

	@CURD(label = "备注", showInList = true, showInView = false, required = false, insertConf = @InsertConf(true), updateConf = @UpdateConf(true))
	protected String remarks; // 备注

	public BaseVo() {
	}

	public BaseVo(ID id) {
		this.id = id;
	}

	public ID getId() {
		return id;
	}

	public void setId(ID id) {
		this.id = id;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(String delFlag) {
		this.delFlag = delFlag;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ReflectionToStringBuilder.toString(this);
	}

}
