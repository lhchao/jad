package com.jad.cache.ehcache;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import com.jad.cache.common.AbstractManageredCache;
import com.jad.cache.common.CacheClient;

/**
 * 封装ehcache
 * 
 * 此类的每个实例就是ehcache.xml中每个cache配置
 *	
 */
public class JadEhCacheCache extends AbstractManageredCache {
	
	private static final Logger logger = LoggerFactory.getLogger(JadEhCacheCache.class); 
	
	/**
	 * 具体的cache实例
	 */
	private final Ehcache cache;
	
	public JadEhCacheCache(CacheClient cacheClient,Ehcache ehcache,int activityTime,boolean allowNullValues) {
		super(cacheClient,activityTime,allowNullValues);
		
		Assert.notNull(ehcache, "cache不能为null");
		Status status = ehcache.getStatus();
		Assert.isTrue(Status.STATUS_ALIVE.equals(status),"必须为alive状态的缓存，当前状态:" + status.toString());
		
		this.cache = ehcache;
	}
	
	@Override
	public String getName() {
		return this.cache.getName();
	}

	@Override
	public final Ehcache getNativeCache() {
		return cache;
	}

	
	@Override
	protected Object lookup(Object key) {
		if(key==null){
			logger.warn("无法从缓存中获取key为null的值,"+this.getLoggerInfo( null));
			return null;
		}
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法从缓存中获得数据,"+this.getLoggerInfo(key==null?null:key.toString()));
			return null;
		}
		Element element = this.cache.get(key);
		return element == null ? null : element.getObjectValue();
	}


	
	
	@Override
	public void put(Object key, Object value) {
		put(key,value,this.getActivityTime());
	}
	
	public void put(Object key, Object value,int activityTime){
		if(key==null){
			logger.warn("key为null,不能缓存对像,"+this.getLoggerInfo(null));
			return ;
		}
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把对像保存到缓存中,"+this.getLoggerInfo(key==null?null:key.toString()));
			return ;
		}
		
		if(!super.isAllowNullValues() && value == null ){
			logger.warn("不能把null对像整到缓存中,"+getLoggerInfo(key.toString()));
			return ;
		}
		
		logger.debug("准备缓存对像,有效秒数:"+activityTime+","+this.getLoggerInfo(key.toString()));
		
		this.cache.put(new Element(key, toStoreValue(value,activityTime)));
	}

	@Override
	public ValueWrapper putIfAbsent(Object key, Object value) {
		if(key==null){
			logger.warn("key为null,不能缓存对像,"+this.getLoggerInfo(null));
			return null;
		}
		
		if(!super.isAllowNullValues() && value == null ){
			logger.warn("不能把null对像整到缓存中,"+getLoggerInfo(key.toString()));
			return null;
		}
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把对像保存到缓存中,"+this.getLoggerInfo(key.toString()));
			return null;
		}
		logger.debug("准备缓存对像,有效秒数:"+this.getActivityTime()+","+this.getLoggerInfo(key.toString()));
		Object existing = this.cache.putIfAbsent(new Element(key, toStoreValue(value,this.getActivityTime())));
		return toValueWrapper(existing,key);
	}

	@Override
	public void evict(Object key) {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法把对像从缓存中删除,"+this.getLoggerInfo(key==null?null:key.toString()));
			return ;
		}
		logger.debug("准备清除对像,"+this.getLoggerInfo(key.toString()));
		this.cache.remove(key);
	}



	@Override
	public void clear() {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法清空缓存,"+this.getLoggerInfo(null));
			return ;
		}
		logger.debug("准备清空对像,"+this.getLoggerInfo(null));
		this.cache.removeAll();
	}


	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Set keySet() {
		Set set=new HashSet();
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法获得keyset,"+this.getLoggerInfo(null));
			return set;
		}
		List<String>list=cache.getKeys();
		if(list!=null && !list.isEmpty()){
			set.addAll(list);
		}
		return set;
	}

	@Override
	public Integer size() {
		if(!isClientStarted()){
			logger.debug("缓存还没有启动，无法统计size,"+this.getLoggerInfo(null));
			return 0;
		}
		int size = cache.getSize();
		logger.debug("统计出对像总数:"+size+","+this.getLoggerInfo(null));
		return size;
	}



}
