package com.jad.cache.ehcache;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.util.StringUtils;

import com.jad.cache.common.AbstractCacheClient;
import com.jad.cache.common.AbstractCacheClientManager;
import com.jad.cache.common.JadCacheManager;

/**
 * ehcache客户端
 *
 */
public class EhcacheClient extends AbstractCacheClient {
	
	/**
	 * ehcache实现的缓存管理器名
	 */
	private static final String EH_CACHE_MANAGER_NAME="ehcacheManager";
	
	private static final String EH_CACHE_MANAGER_IMPL_NAME="ehcacheManagerImpl";
	
	private static final String CONFIG_LOCATION_BEAN_NAME="configLocation";
	
	/**
	 * 配置文件路径
	 */
	private String configFile;
	
	
	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		super.postProcessBeanDefinitionRegistry(registry);
		if(!this.isAutoStart()){
			this.stop(false);
		}
	}
	
	
	@Override
	protected void registryCacheManager(BeanDefinitionRegistry registry) {
		
		String ehCacheManagerFactoryBean=this.getClientName()+"_" + EH_CACHE_MANAGER_NAME;
		if (!registry.containsBeanDefinition(ehCacheManagerFactoryBean)) {
			RootBeanDefinition beanDefinition=new RootBeanDefinition(EhCacheManagerFactoryBean.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			if(StringUtils.hasText(this.getConfigFile())){
				beanDefinition.getPropertyValues().add(CONFIG_LOCATION_BEAN_NAME, this.getConfigFile().trim());
			}
			registry.registerBeanDefinition(ehCacheManagerFactoryBean, beanDefinition);
		}
		
		String ehCacheManagerBean=this.getClientName()+"_" + EH_CACHE_MANAGER_IMPL_NAME;
		if (!registry.containsBeanDefinition(ehCacheManagerBean)) {
			RootBeanDefinition beanDefinition=new RootBeanDefinition(JadEhCacheCacheManager.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			beanDefinition.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
			beanDefinition.getPropertyValues().add(AbstractCacheClientManager.CACHE_MANAGER_FIELD_NAME, new RuntimeBeanReference(ehCacheManagerFactoryBean));
			beanDefinition.getPropertyValues().add("cacheClient", this);
			registry.registerBeanDefinition(ehCacheManagerBean, beanDefinition);
		}
		
		
		JadCacheManager manager = (JadCacheManager)applicationContext.getBean(ehCacheManagerBean);
		this.setCacheManager(manager);
		registryToMasterCacheManager(manager);//注册到主管理器
		
		
	}

	public String getConfigFile() {
		return configFile;
	}

	public void setConfigFile(String configFile) {
		this.configFile = configFile;
	}




}
