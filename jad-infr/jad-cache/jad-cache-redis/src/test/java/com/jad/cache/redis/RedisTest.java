package com.jad.cache.redis;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class RedisTest {

	private static Logger logger=LoggerFactory.getLogger(RedisTest.class);

	protected ApplicationContext context ;
	
	@Test
    public void testCache() throws Exception {
//		MemCacheClientManager manager = new MemCacheClientManager();
//		manager.start();
		
		AccountService accountService = (AccountService)context.getBean("accountService");
		logger.debug("调用第一次。。。");
		Account a = accountService.getAccountByName("tttsd");
		
//		AccountCao accountCao = (AccountCao)context.getBean("accountCao");
//		
//		AccountCao2 accountCao2 = (AccountCao2)context.getBean("accountCao2");
		
//		Collection<String>names=CacheClientHelper.getInstance().getClientNames();
//		String name = names.iterator().next();
//		CacheClientHelper.getInstance().stop(name);
		
		logger.debug("调用第二次。。。");
		Account a2 = accountService.getAccountByName("tttsd");
		
//		CacheClientHelper.getInstance().start(name);
//		
//		logger.debug("调用第三次。。。");
//		accountService.getAccountByName("testUserNamed");
//		
//		logger.debug("调用第四次。。。");
//		accountService.getAccountByName("testUserNamed");
//		
//		logger.debug("调用第五次。。。");
//		accountService.getAccountByName2("testUserNamedddff5");
		
////		
//		logger.debug("调用第三次。。。");
//		accountCao2.getAccountByName("testUserNamedddff");
//		
		
		
		logger.debug("测试通过。。。");
		
		
	}
	

	
	@BeforeTest
    public void baseBeforeTest() throws Exception {
		logger.debug("context 正在初始化...");
		context = new ClassPathXmlApplicationContext(getContextConfigFile());
		logger.debug("context 初始化完成...");
	}
	
	public String getContextConfigFile() {
		return "spring-cache-redis.xml";
	}

}
