package com.jad.cache.common;

/**
 * 缓存异常封装
 * @author hechuan
 */
public class CacheException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CacheException() {
		// TODO Auto-generated constructor stub
	}

	public CacheException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CacheException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CacheException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
