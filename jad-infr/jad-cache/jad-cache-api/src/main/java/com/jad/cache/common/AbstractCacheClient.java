package com.jad.cache.common;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.StringUtils;

/**
 * 抽像缓存客户端实现 
 * 
 * 具体的缓存厂商的client实现可从此类继承
 * 
 * 此类的实例在初始化的时候，会自动向spring容器中注用它所控制的CacheManager，同时初始化它所管理的Cache实例
 * 
 * 此类可以配置设定几个诸如 allowNullValues之类的属性，用于通过CacheManager操作Cache时的默认依具
 * 而且这些属性在管理它的CacheClientManager实例中也有相应配置，所以此实例的这些属性可以缺省配置，而直接使用CacheClientManager中的全局配置
 * 
 * @author Administrator
 *
 */
public abstract class AbstractCacheClient implements CacheClient,
	BeanDefinitionRegistryPostProcessor,DisposableBean,ApplicationContextAware,BeanNameAware{
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractCacheClient.class);
	
	protected ApplicationContext applicationContext;
	
	/**
	 * 缓存管理器
	 */
	private JadCacheManager cacheManager;
	
	/**
	 * 默认缓存存活时间秒数
	 * 默认采用全局配置
	 */
	private Integer defActivityTime = null;
	
	/**
	 * 是否可以缓存null
	 * 默认采用全局配置
	 */
	private Boolean allowNullValues = null ;
	
	/**
	 * 是否自动创建缓存
	 * 默认采用全局配置
	 */
	private Boolean autoCreateCache = null;
	
	/**
	 * 客户端名称
	 */
	private String clientName;
	
	
	/**
	 * 此客户端可以使用的缓存
	 */
	private Collection<String>cacheNames;
	
	/**
	 * 同 cacheNames 
	 */
	private Collection<JadCache>cacheBeans;
	
	/**
	 * 是否自动启动，默认为true
	 */
	private Boolean autoStart = true;
	
	/**
	 * 是否启动，用于标识此客户端管理器是否启动
	 */
	protected volatile Boolean started = false;

	
	


	@Override
	public synchronized void start() {
		if(getCacheManager()==null){
			throw new CacheException("缓存客户端无法启动，因为没有注入缓存管理器,"+this.getLoggerInfo(null, null));
		}
		if(this.isStarted()){
			logger.warn("缓存已经启动，不重复启动,"+this.getLoggerInfo(null, null));
			return;
		}
		logger.debug("缓存客户端启动,"+this.getLoggerInfo(null, null));
		started = true;
	}

	@Override
	public void stop() {
		stop(true);
	}
	
	public synchronized void stop(boolean clear) {

		if(clear){
			Collection<String>names=cacheManager.getCacheNames();
			if(names!=null && !names.isEmpty()){
				for(String name:names){
					cacheManager.getCache(name).clear();
				}
			}
		}
		started = false;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext)throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public void destroy() throws Exception {
		this.stop(false);
	}
	
	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		
		registryCacheManager(registry);//注册缓存管理器
		
		initCache(registry); //初始化被管理的缓存配置
		
		if(this.isAutoStart()){ //自动启动
			this.start();
		}
	}
	
	

	@Override
	public void setBeanName(String beanName) {
		if (!StringUtils.hasText(this.getClientName())) {
			this.setClientName(beanName);
		}
	}
	
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		//这个方法不需要实现
	}
	
	/**
	 * 初始化被管理的缓存配置
	 * @param registry
	 */
	protected synchronized void initCache(BeanDefinitionRegistry registry){
		if(this.getCacheNames()==null || this.getCacheNames().isEmpty()){
			return;
		}
		
		if(this.getCacheBeans()==null){
			this.setCacheBeans(new HashSet<JadCache>());
		}
		
		Map<String,JadCache>alreadyMap=new HashMap<String,JadCache>();
		for(JadCache jc:this.getCacheBeans()){
			if(!StringUtils.hasText(jc.getName())){
				throw new CacheException("请在缓存客户端配置:"+this.getClientName()+"中的cacheBean列表指定每个元素的name属性");
			}
			JadCache old = alreadyMap.put(jc.getName().trim(), jc);
			if(old!=null){
				throw new CacheException("重复的cacheBean配置,clientName:"+this.getClientName()+",name:"+jc.getName());	
			}
		}
		if(this.getCacheNames()!=null){
			for(String name:this.getCacheNames()){
				if(!StringUtils.hasText(name)){
					throw new CacheException("请在缓存客户端配置:"+this.getClientName()+"中的cacheNames列表指定每个元素的name属性");
				}
				if(alreadyMap.containsKey(name.trim())){
					logger.debug("存在重复的cache配置,"+this.getLoggerInfo(name, null));
					continue;
				}
				alreadyMap.put(name.trim(), newJadCache(name.trim()));
			}
		}
		
		if(alreadyMap.isEmpty()){
			logger.debug("本客户端没有配置任何可管理的cache,"+this.getLoggerInfo(null, null));
			return;
		}
		for(JadCache jc:alreadyMap.values()){
			this.getCacheManager().initCache(jc);
		}
		
	}
	
	private JadCache newJadCache(String name){
		JadCache jc = new JadCache();
		jc.setName(name);
		
		if(this.getCacheManager() instanceof JadAbstractCacheManager){
			CacheClient client = ((JadAbstractCacheManager)this.getCacheManager()).getCacheClient();
			jc.setAllowNullValues(client.isAllowNullValues());
			jc.setDefActivityTime(client.getDefActivityTime() == null ? -1 :client.getDefActivityTime().intValue());
		}
		
		return jc;
	}
	
	/**
	 * 注册缓存管理器
	 * @param registry
	 */
	protected abstract void registryCacheManager(BeanDefinitionRegistry registry);
	
	/**
	 * 检查缓存是否启动
	 * @return
	 */
	protected boolean checkStart(){
		if(!this.isStarted()){
			logger.warn("缓存没有启动,clientName:"+this.getClientName());
			return false;
		}
		return true;
	}
	
	/**
	 * 注册到主管理器
	 * @param cacheManager
	 */
	protected void registryToMasterCacheManager(JadCacheManager cacheManager){
		
		Object object = applicationContext.getBean(AbstractCacheClientManager.CACHE_MANAGER_BEAN_NAME);
		
		if(object instanceof MasterCacheManager){
			
			MasterCacheManager manager = (MasterCacheManager)object;
			manager.registry(this, cacheManager);
			
		}else{
			logger.warn("context中找不到CompositeCacheManager类型的缓存管理器,"+this.getLoggerInfo(null, null));
		}
	}

	
	private String getLoggerInfo(String cacheName,String key){
		StringBuffer sb=new StringBuffer();
		sb.append("clientName:").append(this.getClientName());
		if(StringUtils.hasText(cacheName)){
			sb.append(",cacheName:").append(cacheName);
		}
		if(StringUtils.hasText(key)){
			sb.append(",key:").append(key);
		}
		return sb.toString();
	}
	
	@Override
	public boolean isStarted() {
		return started==null?false:started.booleanValue();
	}

	public JadCacheManager getCacheManager() {
		return cacheManager;
	}

	public void setCacheManager(JadCacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}


	public Integer getDefActivityTime() {
		return defActivityTime;
	}

	public void setDefActivityTime(Integer defActivityTime) {
		this.defActivityTime = defActivityTime;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	
	public Collection<String> getCacheNames() {
		return cacheNames;
	}

	public void setCacheNames(Collection<String> cacheNames) {
		this.cacheNames = cacheNames;
	}

	public boolean isAutoStart() {
		return autoStart ==null ? false:autoStart.booleanValue();
	}
	public Boolean getAutoStart() {
		return autoStart;
	}

	public void setAutoStart(Boolean autoStart) {
		this.autoStart = autoStart;
	}

	public Boolean getAllowNullValues() {
		return allowNullValues;
	}
	
	public boolean isAllowNullValues() {
		return allowNullValues == null ? false : allowNullValues.booleanValue();
	}
	
	public void setAllowNullValues(Boolean allowNullValues) {
		this.allowNullValues = allowNullValues;
	}

	public boolean isAutoCreateCache() {
		return autoCreateCache == null ? false : autoCreateCache.booleanValue();
	}
	
	public Boolean getAutoCreateCache() {
		return autoCreateCache;
	}

	public void setAutoCreateCache(Boolean autoCreateCache) {
		this.autoCreateCache = autoCreateCache;
	}

	public Collection<JadCache> getCacheBeans() {
		return cacheBeans;
	}

	public void setCacheBeans(Collection<JadCache> cacheBeans) {
		this.cacheBeans = cacheBeans;
	}
	
	
}
