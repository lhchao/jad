package com.jad.cache.common;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.Assert;


/**
 * 抽像缓存客户端管理器实现
 * 
 * 具体的缓存厂商的ClientManager实现可从此类继承
 * 
 * 此类的实例时在srping容器初始化时，自动初始化他管理的所有 CacheClient，以及对它他进行简单验证
 * 
 * 此类可以配置设定几个诸如 allowNullValues之类的全局属性，用于作为CacheClient中缺省配置时的默认值
 *
 */
public abstract class AbstractCacheClientManager implements CacheClientManager,
	BeanDefinitionRegistryPostProcessor,InitializingBean,DisposableBean,ApplicationContextAware{
	
	private static final Logger logger = LoggerFactory.getLogger(AbstractCacheClientManager.class);
	
	private ApplicationContext applicationContext;
	
	/**
	 * 
	 */
	public static final String CACHE_MANAGER_BEAN_NAME="cacheManager";
	
	public static final String CACHE_MANAGER_FIELD_NAME="cacheManager";
	
	
	/**
	 * 客户端列表
	 * key:客户端名称,val:CacheClient
	 */
	private Map<String,CacheClient> cacheClients;
	
	
	/**
	 * 默认缓存存活时间秒数
	 */
//	private Integer defActivityTime = -1;
	private Integer defActivityTime = 600;//默认存活10分钟
	
	/**
	 * 是否可缓存null值
	 * 全局配置，默认为true
	 */
	private Boolean allowNullValues = true;
	
	/**
	 * 是否自动创建缓存
	 * 全局配置，默认为true
	 */
	private Boolean autoCreateCache = true;
	
	
	public AbstractCacheClientManager(){
		cacheClients = new HashMap<String,CacheClient>();
	}
	
	
	@Override
	public void afterPropertiesSet() throws Exception {
		
		initClients();//初始化所有客户端
		
		validateClients();//验证
		
	}
	
	/**
	 * 初始化所管理的所有客户端
	 */
	protected abstract void initClients();
	
	
	/**
	 * 初始化一个客户端
	 */
	protected void initClient(CacheClient client){
		if(client.getDefActivityTime()==null){
			client.setDefActivityTime(this.getDefActivityTime()==null?Integer.valueOf(0):this.getDefActivityTime().intValue());
		}
		if(client.getAllowNullValues() == null){
			client.setAllowNullValues(this.isAllowNullValues());
		}
		if(client.getAutoCreateCache() == null ){
			client.setAutoCreateCache(this.isAutoCreateCache());
		}
	}
	
	
	/**
	 * 注册主管理器
	 * @param registry
	 */
	protected abstract void registerMasterManagerIfNecessary(BeanDefinitionRegistry registry);

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		registerMasterManagerIfNecessary(registry);
	}
	

	@Override
	public void setApplicationContext(ApplicationContext applicationContext)throws BeansException {
		this.applicationContext = applicationContext;
	}

	@Override
	public void destroy() throws Exception {
//		for(CacheClient client:this.getCacheClients().values()){
//			client.stop();
//		}
	}

	

	@Override
	public CacheClient getCacheClient(String clientName) {
		Assert.notNull(clientName);
		CacheClient client = this.getCacheClients().get(clientName);
		if(client == null){
			throw new CacheException("没有获得名称为["+clientName+"]的缓存客户端");
		}
		return client;
	}

	@Override
	public Map<String,CacheClient> getCacheClients(){
		return cacheClients;
	}

	@Override
	public void start(CacheClient client) {
		client.start();
	}

	@Override
	public void stop(CacheClient client) {
		client.stop();
	}

	@Override
	public boolean isStarted(CacheClient client) {
		return client.isStarted();
	}


//	@Override
//	public void clearAll(CacheClient client) {
//		client.clearAll();
//	}

	@Override
	public Integer getDefActivityTime() {
		return defActivityTime;
	}

	public void setDefActivityTime(Integer defActivityTime) {
		this.defActivityTime = defActivityTime;
	}
	
	
	protected void validateClients(){
		if(this.getCacheClients()==null || this.getCacheClients().isEmpty()){
			throw new CacheException("没有配置缓存客户端");
		}
	}
	
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		//  这个方法不需要重写
	}
	
	public static Object getFieldValue(final Object obj, final String fieldName) {
		Field field = getAccessibleField(obj, fieldName);

		if (field == null) {
			throw new IllegalArgumentException("Could not find field [" + fieldName + "] on target [" + obj + "]");
		}

		Object result = null;
		try {
			result = field.get(obj);
		} catch (IllegalAccessException e) {
			logger.error("不可能抛出的异常{}", e.getMessage(),e);
		}
		return result;
	}
	
	public static Field getAccessibleField(final Object obj, final String fieldName) {
		Assert.notNull(obj);
		Assert.notNull(fieldName);
		for (Class<?> superClass = obj.getClass(); superClass != Object.class; superClass = superClass.getSuperclass()) {
			try {
				Field field = superClass.getDeclaredField(fieldName);
				makeAccessible(field);
				return field;
			} catch (NoSuchFieldException e) {//NOSONAR
				// Field不在当前类定义,继续向上转型
				continue;// new add
			}
		}
		return null;
	}
	
	public static void makeAccessible(Field field) {
		if ((!Modifier.isPublic(field.getModifiers()) || !Modifier.isPublic(field.getDeclaringClass().getModifiers()) || Modifier
				.isFinal(field.getModifiers())) && !field.isAccessible()) {
			field.setAccessible(true);
		}
	}
	
	
	
	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	@Override
	public boolean isAllowNullValues(){
		
		return this.allowNullValues == null ? false : this.allowNullValues.booleanValue();
	}
	
	public Boolean getAllowNullValues() {
		return allowNullValues;
	}

	public void setAllowNullValues(Boolean allowNullValues) {
		this.allowNullValues = allowNullValues;
	}

	@Override
	public boolean isAutoCreateCache() {
		return autoCreateCache == null ? false : this.getAutoCreateCache().booleanValue();
	}
	
	public Boolean getAutoCreateCache() {
		return autoCreateCache;
	}

	public void setAutoCreateCache(Boolean autoCreateCache) {
		this.autoCreateCache = autoCreateCache;
	}
	


}




