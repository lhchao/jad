package com.jad.cache.common;

import java.io.Serializable;
import java.util.Date;

import org.springframework.cache.support.SimpleValueWrapper;


/**
 * 缓存中的实际对像
 * 本缓存框架中，被缓存的数据都被包装成此类的实例
 * 它包含一个Date类型的expiryTime，表示使对象的过期时间
 * @author Administrator
 *
 */
public class CacheValue implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * 失效时间
	 * 为空时忽略本属性
	 * 注意：这仅仅是一个业务上的失效概念
	 * 当系统时间晚于此时间时，从业务上看，算是对像过期，但它可能还存在于缓存中(这取决于ehcache或memcache自身的失效时间)
	 * 业务系统在在从缓存中取出对像时，需要再自行通过此字段判断是否过期
	 */
	private long expiryTime;
	
	
	private Object value;
	
	public CacheValue() {
	}
	
	public CacheValue(Object value) {
		this.value = value;
	}
	
	public CacheValue(Object value,long expiryTime) {
		this.value = value;
		this.expiryTime = expiryTime;
	}
	
	


	public Object get() {
		return this.value;
	}
	
	/**
	 * 是否为null
	 * @param value
	 * @return
	 */
	public static boolean isNull(CacheValue value){
		return value == null;
	}
	
	/**
	 * 是否为null
	 * @param value
	 * @return
	 */
	public static boolean isNullData(CacheValue value){
		return isNull(value) || value.get() == null;
	}
	
	/**
	 * 是否失效
	 * @return
	 */
	public static boolean isExpiry(CacheValue value){
		if(value.expiryTime <= 0){
			return false;
		}
		return new Date().after(new Date(value.expiryTime));
	}
	
	public long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(long expiryTime) {
		this.expiryTime = expiryTime;
	}
	
	


}
