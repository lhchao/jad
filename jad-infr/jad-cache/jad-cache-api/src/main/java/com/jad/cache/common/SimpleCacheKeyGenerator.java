package com.jad.cache.common;

import java.lang.reflect.Method;

import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.util.StringUtils;


/**
 * 系统默认的KeyGenerator，用于通过 spring cache 注解使用缓存时的缺省key生成器
 * 
 * 作为缺省的KeyGenerator，这并不是一个完美的KeyGenerator，用户可以自已跟据当前的业务情况自己写一个
 * 自定义的KeyGenerator需要配置到相关配置文件的cache:annotation-driven 标签中它的key-generator属性
 *  
 */
public class SimpleCacheKeyGenerator implements KeyGenerator {

//	private static final Logger logger = LoggerFactory.getLogger(SimpleCacheKeyGenerator.class);
	
	private static final String EMPTY_PARAMS_KEY="keyWithEmptyparams";
	
	// 因本框架从jad主项目了分离出来，以下业务相关的常量不再使用
//	private static final String DEFAULT_KEY_FIELD_NAME="id"; 
//	private static final String PAGE_CACHE_FLAG = "page";
//	private static final String QO_CACHE_FLAG = "qo";
//	private static final String KEY_SEPARATOR = Constants.CACHE_KEY_SEPARATOR;
//	private static final String LIST_CACHE_FLAG = "list";
	
	
	/**
	 * 重新实现生成主键的方法
	 */
	public Object generate(Object target, Method method, Object... params) {
		return generateKey(params);
	}
	
	public String generateKey(Object... params) {
		if (params.length == 0) {
			return EMPTY_PARAMS_KEY;
		}
		if (params.length == 1) {
			Object param = params[0];
			if (param != null && !param.getClass().isArray()) {
				return param.toString();
			}
		}
		return StringUtils.arrayToCommaDelimitedString(params);
	}
	
	
	
	// 因本框架从jad主项目了分离出来，以下业务相关的方法不再使用
//	@Override
//	public Object generate(Object target, Method method, Object... params) {
//		StringBuffer sb = new StringBuffer();
//		if(params==null){
//			return sb.toString();
//		}
//		for(Object obj:params){
//			if(obj==null){
//				continue;
//			}
//			if(obj instanceof Page){
//				sb.append(convertToKeyForCache((Page)obj));
//				sb.append(KEY_SEPARATOR);
//				
//			} else if(obj instanceof QueryObject){
//				sb.append(convertToKeyForCache((QueryObject)obj));
//				sb.append(KEY_SEPARATOR);
//				
//			} else if(obj instanceof List){
//				sb.append(convertToKeyForCache((List)obj));
//				sb.append(KEY_SEPARATOR);
//				
//			}else{
//				sb.append(StringUtils.deleteWhitespace(obj.toString()));
//			}
//		}
//		return sb.toString();
//	}
	
	
//	@Override
//	public <CO extends CacheObject> String generate(CO t){
//		if(t==null){
//			return null;
//		}
//		Field field = ReflectionUtils.getAccessibleField(t, DEFAULT_KEY_FIELD_NAME);
//		
//		if(field!=null){
//			Object keyObj;
//			try {
//				keyObj = field.get(t);
//				if(keyObj!=null){
//					return keyObj.toString();
//				}
//			}catch (Exception e) {
//				logger.error("无法通过co["+t.getClass().getName()+"]获得缓存key的值"+e.getMessage(),e);
//			}
//			
//		}
//		return null;
//		
//	}
	
//	@SuppressWarnings("rawtypes")
//	private static String convertToKeyForCache(List idList){
//		if(idList==null || idList.isEmpty()){
//			return "";
//		}
//		StringBuffer sb=new StringBuffer();
//		sb.append(LIST_CACHE_FLAG);
//		for(Object id:idList){
//			if(id!=null){
//				sb.append(KEY_SEPARATOR);
//				sb.append(id.toString());
//			}
//		}
//		return sb.toString();
//	}
	
	
//	@SuppressWarnings("unchecked")
//	private static <QO extends QueryObject> String convertToKeyForCache(QO qo){
//		if(qo==null){
//			return StringUtils.EMPTY;
//		}
//		QoMetaInfo<QO> qoMeta = EntityUtils.getQoInfo((Class<QO>)qo.getClass());
//		StringBuffer sb = new StringBuffer();
//		sb.append(QO_CACHE_FLAG).append(KEY_SEPARATOR);
//		sb.append(qoMeta.getMetaClass().getSimpleName());
//		for(Map.Entry<String, QoFieldInfo<QO>> ent : qoMeta.getFieldInfoMap().entrySet()){
//			Object v = ReflectionUtils.getFieldValue(qo, ent.getKey());
//			if(v!=null){
//				sb.append(KEY_SEPARATOR).append(ent.getKey()).append(v);
//			}
//		}
//		return sb.toString();
//	}
	
	
//	private static String convertToKeyForCache(Page page){
//		if(page==null){
//			return StringUtils.EMPTY;
//		}
//		StringBuffer sb = new StringBuffer();
//		sb.append(PAGE_CACHE_FLAG).append(page.getPageNo()<=0?1:page.getPageNo());
//		sb.append(page.getPageSize());
//		
//		return sb.toString();
//	}
	
}


