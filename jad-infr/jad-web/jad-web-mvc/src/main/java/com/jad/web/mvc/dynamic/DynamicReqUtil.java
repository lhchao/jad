package com.jad.web.mvc.dynamic;

import com.jad.web.mvc.dynamic.exception.DynamicReqException;

public class DynamicReqUtil {

	private DynamicReqUtil() {
	}
	
	public static DynamicReqAttr findDynamicReqAttr(String name){
		if(name==null){
			throw new DynamicReqException(name+"不能为空");
		}
		DynamicReqAttr dynamicReqAttr = DynamicReqScannerConfigurer.findDynamicReqAttr(name);
		
		return dynamicReqAttr;
	}

}
