package com.alibaba.dubbo;

import javax.servlet.ServletContextEvent;

import com.alibaba.citrus.webx.context.WebxComponentsContext;
import com.alibaba.citrus.webx.context.WebxComponentsLoader;
import com.alibaba.citrus.webx.context.WebxContextLoaderListener;

public class JadWebxContextLoaderListener extends WebxContextLoaderListener{
	
	public void contextInitialized(ServletContextEvent event) {
		
		WebxComponentsLoader componentsLoader=new WebxComponentsLoader() {
            @Override
            protected Class<? extends WebxComponentsContext> getDefaultContextClass() {
                Class<? extends WebxComponentsContext> defaultContextClass = JadWebxContextLoaderListener.this
                        .getDefaultContextClass();

                if (defaultContextClass == null) {
                    defaultContextClass = super.getDefaultContextClass();
                }

                return defaultContextClass;
            }
        };
        componentsLoader.initWebApplicationContext(event.getServletContext());
		
	}
}
