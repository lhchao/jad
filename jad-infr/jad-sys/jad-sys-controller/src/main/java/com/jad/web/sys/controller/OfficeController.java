/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.sys.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Constants;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.util.TreeUtil;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.web.api.Result;
import com.jad.core.sys.qo.OfficeQo;
import com.jad.core.sys.service.DictService;
import com.jad.core.sys.service.OfficeService;
import com.jad.core.sys.service.UserService;
import com.jad.core.sys.util.MenuUtil;
import com.jad.core.sys.vo.OfficeVo;
import com.jad.core.sys.vo.UserVo;
import com.jad.web.mvc.BaseController;

/**
 * 机构Controller
 * @author ThinkGem
 * @version 2013-5-15
 */
@Controller
@RequestMapping(value = "${adminPath}/sys/office")
@Api(description = "机构管理")
public class OfficeController extends BaseController {

	@Autowired
	private OfficeService officeService;
	
	@Autowired
	private DictService dictService;
	
	@Autowired
	private UserService userService;
	
//	@ModelAttribute("office")
//	public OfficeVo get(@RequestParam(required=false) String id) {
//		if (StringUtils.isNotBlank(id)){
//			return officeService.findById(id);
//		}else{
//			return new OfficeVo();
//		}
//	}
	
	
	/**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:office:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看机构信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<OfficeVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(officeService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @return
	 */
	@RequiresPermissions("sys:office:view")
	@ResponseBody
	@RequestMapping(value = {"findList"})
	@ApiOperation(value = "查询机构列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<List<OfficeVo>> findList(OfficeQo officeQo){
		List<OfficeVo> list =  new ArrayList<OfficeVo>();
		List<OfficeVo> sourcelist = officeService.findList(officeQo);
		TreeUtil.sortList(list, sourcelist, MenuUtil.getRootId(), true);
		return Result.newSuccResult(list);
	}
	
	/**
	 * 修改
	 * @param officeVo
	 * @return
	 */
	@RequiresPermissions("sys:office:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改机构信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(OfficeVo officeVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, officeVo)){
			return result;
		}
		if(StringUtils.isBlank(officeVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		preUpdate(officeVo,SecurityHelper.getCurrentUserId());
		officeService.update(officeVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:office:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增会员信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(OfficeVo officeVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, officeVo)){
			return result;
		}
		preInsert(officeVo,SecurityHelper.getCurrentUserId());
		officeService.add(officeVo);
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:office:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除机构信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		OfficeVo delVo = new OfficeVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		officeService.delete(delVo);
		return result;
	}
	
	

	@RequiresPermissions("sys:office:view")
	@RequestMapping(value = {""})
	public String index(OfficeQo officeQo, Model model) {
		return "modules/sys/officeIndex";
	}

	@RequiresPermissions("sys:office:view")
	@RequestMapping(value = {"list"})
	public String list(OfficeQo officeQo, Model model) {
		List<OfficeVo>voList=officeService.findList(officeQo);
        model.addAttribute("list", voList);
		return "modules/sys/officeList";
	}
	
//	private OfficeQo getQo(OfficeVo officeVo){
//		OfficeQo qo = new OfficeQo();
//		qo.setId(officeVo.getId());
//		qo.setGrade(officeVo.getGrade());
//		qo.setType(officeVo.getType());
//		return qo;
//	}
	
	@RequiresPermissions("sys:office:view")
	@RequestMapping(value = "form")
	public String form(OfficeQo officeQo, Model model) {
//		UserVo user = systemService.getUser();
//		if (office.getParent()==null || office.getParent().getId()==null){
//			office.setParent(user.getOffice());
//		}
//		office.setParent(officeService.get(office.getParent().getId()));
//		if (office.getArea()==null){
//			office.setArea(user.getOffice().getArea());
//		}
//		// 自动获取排序号
//		if (StringUtils.isBlank(officeVo.getId())&&officeVo.getParent()!=null){
//			int size = 0;
//			List<OfficeVo> list = officeService.findAll();
//			for (int i=0; i<list.size(); i++){
//				OfficeVo e = list.get(i);
//				if (e.getParent()!=null && e.getParent().getId()!=null
//						&& e.getParent().getId().equals(officeVo.getParent().getId())){
//					size++;
//				}
//			}
//			officeVo.setCode(officeVo.getParent().getCode() + StringUtils.leftPad(String.valueOf(size > 0 ? size+1 : 1), 3, "0"));
//		}
		
		OfficeVo officeVo = new OfficeVo();
		
		UserVo currentUser = SecurityHelper.getCurrentUser();
		OfficeVo currentOffice = currentUser.getOffice();
		if(currentOffice!=null && StringUtils.isNotBlank(currentOffice.getId()) ){
			currentOffice = officeService.findById(currentUser.getOffice().getId());
			if(currentOffice!=null){
				officeVo.setArea(currentOffice.getArea());
			}
		}
		
		if(StringUtils.isNotBlank(officeQo.getId())){//修改
			
			officeVo = officeService.findById(officeQo.getId());
			if(officeVo.getPrimaryPerson()!=null && officeVo.getPrimaryPerson().getId()!=null){
				officeVo.setPrimaryPerson(userService.findById(officeVo.getPrimaryPerson().getId()));
			}
			if(officeVo.getDeputyPerson()!=null && officeVo.getDeputyPerson().getId()!=null){
				officeVo.setDeputyPerson(userService.findById(officeVo.getDeputyPerson().getId()));
			}
			
		}else if(StringUtils.isNotBlank(officeQo.getParentId())){ //添加下级
			
			officeVo.setParent(officeService.findById(officeQo.getParentId()));
			
		}else{ //添加新的
			
			if(currentOffice!=null ){
				
				officeVo.setParent(currentOffice);
			}
			
			
		}

		
		
//		if(StringUtils.isNotBlank(officeQo.getId())){
//			officeVo = officeService.findById(officeQo.getId());
//		}
		
		model.addAttribute("officeVo", officeVo);
		return "modules/sys/officeForm";
	}
	
	@RequiresPermissions("sys:office:edit")
	@RequestMapping(value = "save")
	public String save(OfficeVo officeVo, Model model, RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/office/";
		}
		if (!beanValidator(model, officeVo)){
//			return form(officeVo, model);
//			model.addAttribute("officeVo", officeVo);
			return "modules/sys/officeForm";
		}
		if(StringUtils.isNotBlank(officeVo.getId())){
			preUpdate(officeVo,SecurityHelper.getCurrentUserId());
			officeService.update(officeVo);
		}else{
			preInsert(officeVo,SecurityHelper.getCurrentUserId());
			officeService.add(officeVo);
		}
		
//		if(officeVo.getChildDeptList()!=null){
//			OfficeVo childOffice = null;
//			for(String id : officeVo.getChildDeptList()){
//				childOffice = new OfficeVo();
//				childOffice.setName(dictService.getDictLabel(id, "sys_office_common", "未知"));
//				childOffice.setParent(officeVo);
//				childOffice.setArea(officeVo.getArea());
//				childOffice.setType("2");
//				childOffice.setGrade(String.valueOf(Integer.valueOf(officeVo.getGrade())+1));
//				childOffice.setUseable(Constants.YES);
//				
////				officeService.save(childOffice);
//				if(StringUtils.isNotBlank(childOffice.getId())){
//					officeService.update(childOffice);
//				}else{
//					officeService.add(childOffice);
//				}
//				
//			}
//		}
		
		addMessage(redirectAttributes, "保存机构'" + officeVo.getName() + "'成功");
//		String id = "0".equals(officeVo.getParentId()) ? "" : officeVo.getParentId();
		return "redirect:" + adminPath + "/sys/office/list?parentIds=";
	}
	
	@RequiresPermissions("sys:office:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id,  RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/sys/office/list";
		}
//		if (Office.isRoot(id)){
//			addMessage(redirectAttributes, "删除机构失败, 不允许删除顶级机构或编号空");
//		}else{
		
		OfficeVo delVo = new OfficeVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		officeService.delete(delVo);
		addMessage(redirectAttributes, "删除机构成功");
//		}
//		return "redirect:" + adminPath + "/sys/office/list?id="+officeVo.getParentId()+"&parentIds="+officeVo.getParentIds();
			return "redirect:" + adminPath + "/sys/office/list?parentIds=";
	}

	/**
	 * 获取机构JSON数据。
	 * @param extId 排除的ID
	 * @param type	类型（1：公司；2：部门/小组/其它：3：用户）
	 * @param grade 显示级别
	 * @param response
	 * @return
	 */
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(@RequestParam(required=false) String extId, @RequestParam(required=false) String type,
			@RequestParam(required=false) Long grade, @RequestParam(required=false) Boolean isAll, HttpServletResponse response) {
		List<Map<String, Object>> mapList =  new ArrayList();
		List<OfficeVo> list = officeService.findList(isAll,type);
		for (int i=0; i<list.size(); i++){
			OfficeVo e = list.get(i);
			if ((StringUtils.isBlank(extId) || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1))
					&& (type == null || (type != null && (type.equals("1") ? type.equals(e.getType()) : true)))
					&& (grade == null || (grade != null && Integer.parseInt(e.getGrade()) <= grade.intValue()))
					&& Constants.YES.equals(e.getUseable())){
				Map<String, Object> map =  new HashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("pIds", e.getParentIds());
				map.put("name", e.getName());
				if (type != null && "3".equals(type)){
					map.put("isParent", true);
				}
				mapList.add(map);
			}
		}
		return mapList;
	}
}
