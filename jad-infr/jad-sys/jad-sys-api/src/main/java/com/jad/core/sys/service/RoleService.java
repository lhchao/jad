package com.jad.core.sys.service;

import java.util.List;

import com.jad.commons.service.CurdService;
import com.jad.core.sys.vo.RoleVo;

public interface RoleService extends CurdService<RoleVo,String>{
	
	
	/**
	 * 查询用户所属角色
	 * @param userId
	 * @return
	 */
	public List<RoleVo> findByUserId(String userId);
	
}
