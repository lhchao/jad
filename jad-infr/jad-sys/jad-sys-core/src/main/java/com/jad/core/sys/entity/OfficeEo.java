package com.jad.core.sys.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.jad.commons.entity.TreeEo;
import com.jad.dao.annotation.RelateColumn;
import com.jad.dao.enums.RelateLoadMethod;

@Entity
@Table(name = "sys_office")
public class OfficeEo extends TreeEo<OfficeEo,String>{

	private static final long serialVersionUID = 1L;
	
	private OfficeEo parent;	// 父级编号
	
	private AreaEo area;		// 归属区域
	
	private String code; 	// 机构编码
	private String type; 	// 机构类型（1：公司；2：部门；3：小组）
	private String grade; 	// 机构等级（1：一级；2：二级；3：三级；4：四级）
	private String address; // 联系地址
	private String zipCode; // 邮政编码
	private String master; 	// 负责人
	private String phone; 	// 电话
	private String fax; 	// 传真
	private String email; 	// 邮箱
	private String useable;//是否可用
	
	private UserEo primaryPerson;//主负责人
	private UserEo deputyPerson;//副负责人
	
	
	public OfficeEo(){
		super();
	}

	public OfficeEo(String id){
		super(id);
	}
	
	@Column(name="useable")
	public String getUseable() {
		return useable;
	}

	public void setUseable(String useable) {
		this.useable = useable;
	}

	@ManyToOne
	@RelateColumn(name="primary_person")
	public UserEo getPrimaryPerson() {
		return primaryPerson;
	}

	public void setPrimaryPerson(UserEo primaryPerson) {
		this.primaryPerson = primaryPerson;
	}

	@ManyToOne
	@RelateColumn(name="deputy_person")
	public UserEo getDeputyPerson() {
		return deputyPerson;
	}

	public void setDeputyPerson(UserEo deputyPerson) {
		this.deputyPerson = deputyPerson;
	}

//	@JsonBackReference
//	@NotNull
	@ManyToOne
	@RelateColumn(name="parent_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	public OfficeEo getParent() {
		return parent;
	}
	

	public void setParent(OfficeEo parent) {
		this.parent = parent;
	}
//
//	@Length(min=1, max=2000)
//	public String getParentIds() {
//		return parentIds;
//	}
//
//	public void setParentIds(String parentIds) {
//		this.parentIds = parentIds;
//	}

	@ManyToOne
	@RelateColumn(name="area_id",loadMethod=RelateLoadMethod.LEFT_JOIN)
	@NotNull
	public AreaEo getArea() {
		return area;
	}

	public void setArea(AreaEo area) {
		this.area = area;
	}
//
//	@Length(min=1, max=100)
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public Integer getSort() {
//		return sort;
//	}
//
//	public void setSort(Integer sort) {
//		this.sort = sort;
//	}
	
	@Column(name="type")
	@Length(min=1, max=1)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name="grade")
	@Length(min=1, max=1)
	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	@Column(name="address")
	@Length(min=0, max=255)
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name="zip_code")
	@Length(min=0, max=100)
	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	@Column(name="master")
	@Length(min=0, max=100)
	public String getMaster() {
		return master;
	}

	public void setMaster(String master) {
		this.master = master;
	}

	@Column(name="phone")
	@Length(min=0, max=200)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name="fax")
	@Length(min=0, max=200)
	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	@Column(name="email")
	@Length(min=0, max=200)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="code")
	@Length(min=0, max=100)
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

//	public String getParentId() {
//		return parent != null && parent.getId() != null ? parent.getId() : "0";
//	}
	
//	@Override
//	public String toString() {
//		return name;
//	}



}
