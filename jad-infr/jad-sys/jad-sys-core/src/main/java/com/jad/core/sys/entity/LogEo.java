package com.jad.core.sys.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.jad.commons.entity.BaseEo;

@Entity
@Table(name = "sys_log")
public class LogEo extends BaseEo<String> {

	private static final long serialVersionUID = 1L;
	private String type; 		// 日志类型（1：接入日志；2：错误日志）
	private String title;		// 日志标题
	private String remoteAddr; 	// 操作用户的IP地址
	private String requestUri; 	// 操作的URI
	private String method; 		// 操作的方式
	private String params; 		// 操作提交的数据
	private String userAgent;	// 操作用户代理信息
	private String exception; 	// 异常信息
	private Date beginDate;		// 开始日期
	private Date endDate;		// 结束日期
	
	
	public LogEo(){
		super();
	}
	
	public LogEo(String id){
		super(id);
	}
	
	@Column(name="type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name="title")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name="remote_addr")
	public String getRemoteAddr() {
		return remoteAddr;
	}

	public void setRemoteAddr(String remoteAddr) {
		this.remoteAddr = remoteAddr;
	}

	@Column(name="user_agent")
	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	@Column(name="request_uri")
	public String getRequestUri() {
		return requestUri;
	}

	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}

	@Column(name="method")
	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	@Column(name="params")
	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}
	
	@Column(name="exception")
	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	
	@Column(name="begin_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	@Column(name="end_date")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	
//	/**
//	 * 设置请求参数
//	 * @param paramMap
//	 */
//	@SuppressWarnings({ "unchecked", "rawtypes" })
//	public void setParams(Map paramMap){
//		if (paramMap == null){
//			return;
//		}
//		StringBuilder params = new StringBuilder();
//		for (Map.Entry<String, String[]> param : ((Map<String, String[]>)paramMap).entrySet()){
//			params.append(("".equals(params.toString()) ? "" : "&") + param.getKey() + "=");
//			String paramValue = (param.getValue() != null && param.getValue().length > 0 ? param.getValue()[0] : "");
//			params.append(StringUtils.abbr(StringUtils.endsWithIgnoreCase(param.getKey(), "password") ? "" : paramValue, 100));
//		}
//		this.params = params.toString();
//	}
	

	


}
