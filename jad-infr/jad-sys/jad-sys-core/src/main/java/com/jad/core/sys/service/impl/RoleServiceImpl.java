package com.jad.core.sys.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.service.impl.AbstractServiceImpl;
import com.jad.commons.utils.Validate;
import com.jad.core.sys.dao.RoleDao;
import com.jad.core.sys.dao.UserDao;
import com.jad.core.sys.entity.RoleEo;
import com.jad.core.sys.service.RoleService;
import com.jad.core.sys.vo.RoleVo;
import com.jad.dao.utils.EntityUtils;


@Service("roleService")
@Transactional(readOnly = true)
public class RoleServiceImpl extends AbstractServiceImpl<RoleEo,String, RoleVo> implements RoleService {
	
	@Autowired
	private RoleDao roleDao;
	
	@Autowired
	private UserDao userDao;
	
	public List<RoleVo> findByUserId(String userId){
		Validate.notBlank(userId);
		return EntityUtils.copyToVoList(roleDao.findByUserId(userId), RoleVo.class);
	}
	
	
	/**
	 * 增加 
	 * @param vo
	 */
	@Transactional(readOnly = false)
	public void add(RoleVo vo){
		curdHelper.add(vo);
		
		//添加角色菜单
		if(CollectionUtils.isNotEmpty(vo.getMenuList()) ){
			roleDao.insertRoleMenu(vo.getId(), vo.getMenuList());
		}
		//添加角色部分关联
		if(CollectionUtils.isNotEmpty(vo.getOfficeList())){
			roleDao.insertRoleOffice(vo.getId(), vo.getOfficeList());
		}
		
		
	}
	
	/**
	 * 更新
	 * @param vo
	 */
	@Transactional(readOnly = false)
	public void update(RoleVo vo){
		curdHelper.update(vo);
		if(vo.getMenuList()!=null){
			roleDao.deleteRoleMenu(vo.getId());
			roleDao.insertRoleMenu(vo.getId(), vo.getMenuList());
		}
		if(vo.getOfficeList()!=null){
			roleDao.deleteRoleOffice(vo.getId());
			roleDao.insertRoleOffice(vo.getId(), vo.getOfficeList());
		}
		
	}
	
	
	/**
	 * 删除数据
	 * @param entity
	 */
	@Transactional(readOnly = false)
	public void delete(RoleVo vo){
		curdHelper.delete(vo);
		//删除菜单
		roleDao.deleteRoleMenu(vo.getId());
		//删除角色部门
		roleDao.deleteRoleOffice(vo.getId());
		//删除用户角色
		userDao.deleteUserRoleByRoleId(vo.getId());
	}
	
	
	
	
}



