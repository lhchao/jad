/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.sys.dao;

import java.util.List;

import com.jad.core.sys.entity.DictEo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 字典DAO接口
 * @author ThinkGem
 * @version 2014-05-16
 */
@JadDao
public interface DictDao extends JadEntityDao<DictEo,String> {

	public List<String> findTypeList();
	
}
