package com.jad.entity;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "t_entity_type")
public class TestEntityType extends BaseEntity{
	
	
	
	
//	date            java.util.Date or java.sql.Date  DATE  
//	time            java.util.Date or java.sql.Time  TIME  
//	timestamp       java.util.Date or java.sql.Timestamp TIMESTAMP  
//	calendar        java.util.Calendar            TIMESTAMP  
//	calendar_date   java.util.Calendar            DATE  

//	big_decimal     java.math.BigDecimal       NUMERIC  
	
//	clob            java.sql.Clob               CLOB  
//	blob            java.sql.Blob               BLOB  

	/**
	 * hibernate 默认的日期映射
	 * 映射类型			Java类型								标准SQL类型	描述
		date			java.util.Date或者java.sql.Date		DATE		YYYY-MM-DD
		time			java.util.Date或者java.sql.Time		TIME		HH:MM:SS
		timestamp		java.util.Date或者java.sql.Timestamp	TIMESTAMP	YYYYMMDDHHMMSS
		calendar		java.util.Calendar					TIMESTAMP	YYYYMMDDHHMMSS
		calendar_date	java.util.Calendar					DATE		YYYY-MM-DD
	 * 
	 * 
	 * Java类型			Hibernate映射类型	标准 SQL类型		MySql 类型	Oracle 类型
		byte[]			binary			VARBINARY/BLOB	BLOB		BLOB
		String			text			CLOB			TEXT		CLOB
		serializable	Serializable	VARBINARY/BLOB	BLOB		BLOB
		java.sql.Clob	clob			CLOB			TEXT		CLOB
		java.sql.Blob	blob			BLOB			BLOB		BLOB


		MySQL的四种BLOB类型：
		类型 大小(单位：字节)
		TinyBlob 最大 255
		Blob 最大 65K
		MediumBlob 最大 16M
		LongBlob 最大 4G

	 */
	
	
	@Column(name="clob_type1")//mysql类型为TEXT，oracle中为CLOB
	private String clob1;
	
	
	@Column(name="blob_type1")//mysql类型为TinyBlob,BLOB,MediumBlob,LongBlob.oracle中字段都为BLOB
	private byte[] blob1;
	
	@Column(name="big_integer_type")//mysql中应为bigint,oracle中应为number
	private BigInteger bigInteger;
	
	@Column(name="big_decimal_type")//mysql中应为decimal,oracle中应为number
	private BigDecimal bigDecimal;
	
	@Column(name="util_date_type")//数据库字段类型请用 datetime
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Date utilDate;//如果没有Temporal注解，默认为 DATE
	
	@Column(name="calendar_type")//数据库字段类型请用 datetime
	@Temporal(TemporalType.TIMESTAMP)
	private java.util.Calendar calendar;//如果没有Temporal注解，默认为 TIMESTAMP
	
	@Column(name="sql_date_type")//数据库字段类型请用 date
	private java.sql.Date sqlDate;//默认为 date
	
	@Column(name="sql_time_type")//数据库字段类型请用 time
	private java.sql.Time sqlTime;//默认为 time
	
	@Column(name="timestamp_type")//数据库字段类型请用 timestamp
	private java.sql.Timestamp timestamp;//默认为 TIMESTAMP

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private String id;
	
	@Column(name="boolean_type")//mysql中字段类型推荐为char或varchar
	private boolean booleanType;
	
	@Column(name="boolean_type2")//mysql中字段类型推荐为char或varchar
	private Boolean booleanType2;
	
	@Column(name="char_type")
	private char charType;
	
	@Column(name="char_type2")
	private Character charType2;
	
	@Column(name="byte_type")
	private byte byteType;
	
	@Column(name="byte_type2")
	private Byte byteType2;
	
	@Column(name="short_type")
	private short shortType;
	
	@Column(name="short_type2")
	private Short shortType2;
	
	@OrderBy("int_type asc")
	@Column(name="int_type")
	private int intType;
	
	@Column(name="int_type2")
	private Integer intType2;
	
	@Column(name="long_type")
	private long longType;
	
	@Column(name="long_type2")
	private Long longType2;
	
	@Column(name="float_type")
	private float floatType;
	
	@Column(name="float_type2")
	private Float floatType2;
	
	@Column(name="double_type")
	private double doubleType;
	
	@Column(name="double_type2")
	private Double doubleType2;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isBooleanType() {
		return booleanType;
	}

	public void setBooleanType(boolean booleanType) {
		this.booleanType = booleanType;
	}

	public Boolean getBooleanType2() {
		return booleanType2;
	}

	public void setBooleanType2(Boolean booleanType2) {
		this.booleanType2 = booleanType2;
	}

	public char getCharType() {
		return charType;
	}

	public void setCharType(char charType) {
		this.charType = charType;
	}

	public Character getCharType2() {
		return charType2;
	}

	public void setCharType2(Character charType2) {
		this.charType2 = charType2;
	}

	public byte getByteType() {
		return byteType;
	}

	public void setByteType(byte byteType) {
		this.byteType = byteType;
	}

	public Byte getByteType2() {
		return byteType2;
	}

	public void setByteType2(Byte byteType2) {
		this.byteType2 = byteType2;
	}

	public short getShortType() {
		return shortType;
	}

	public void setShortType(short shortType) {
		this.shortType = shortType;
	}

	public Short getShortType2() {
		return shortType2;
	}

	public void setShortType2(Short shortType2) {
		this.shortType2 = shortType2;
	}

	public int getIntType() {
		return intType;
	}

	public void setIntType(int intType) {
		this.intType = intType;
	}

	public Integer getIntType2() {
		return intType2;
	}

	public void setIntType2(Integer intType2) {
		this.intType2 = intType2;
	}

	public long getLongType() {
		return longType;
	}

	public void setLongType(long longType) {
		this.longType = longType;
	}

	public Long getLongType2() {
		return longType2;
	}

	public void setLongType2(Long longType2) {
		this.longType2 = longType2;
	}

	public float getFloatType() {
		return floatType;
	}

	public void setFloatType(float floatType) {
		this.floatType = floatType;
	}

	public Float getFloatType2() {
		return floatType2;
	}

	public void setFloatType2(Float floatType2) {
		this.floatType2 = floatType2;
	}

	public double getDoubleType() {
		return doubleType;
	}

	public void setDoubleType(double doubleType) {
		this.doubleType = doubleType;
	}

	public Double getDoubleType2() {
		return doubleType2;
	}

	public void setDoubleType2(Double doubleType2) {
		this.doubleType2 = doubleType2;
	}

	public java.util.Date getUtilDate() {
		return utilDate;
	}

	public void setUtilDate(java.util.Date utilDate) {
		this.utilDate = utilDate;
	}

	public java.util.Calendar getCalendar() {
		return calendar;
	}

	public void setCalendar(java.util.Calendar calendar) {
		this.calendar = calendar;
	}

	public java.sql.Date getSqlDate() {
		return sqlDate;
	}

	public void setSqlDate(java.sql.Date sqlDate) {
		this.sqlDate = sqlDate;
	}

	public java.sql.Time getSqlTime() {
		return sqlTime;
	}

	public void setSqlTime(java.sql.Time sqlTime) {
		this.sqlTime = sqlTime;
	}

	public java.sql.Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(java.sql.Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public BigDecimal getBigDecimal() {
		return bigDecimal;
	}

	public void setBigDecimal(BigDecimal bigDecimal) {
		this.bigDecimal = bigDecimal;
	}

	public BigInteger getBigInteger() {
		return bigInteger;
	}

	public void setBigInteger(BigInteger bigInteger) {
		this.bigInteger = bigInteger;
	}

	public String getClob1() {
		return clob1;
	}

	public void setClob1(String clob1) {
		this.clob1 = clob1;
	}

	public byte[] getBlob1() {
		return blob1;
	}

	public void setBlob1(byte[] blob1) {
		this.blob1 = blob1;
	}

	
	
	
}
