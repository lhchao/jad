package com.jad.dao.hibernate.dialect;

import java.sql.Types;

import org.hibernate.Hibernate;
import org.hibernate.dialect.MySQL5InnoDBDialect;
import org.hibernate.type.StandardBasicTypes;

public class MySQLDialect extends MySQL5InnoDBDialect {

	protected void registerVarcharTypes() {
		super.registerVarcharTypes();
		registerColumnType(Types.CHAR, 255, "char($l)");
		registerHibernateType( Types.CHAR, StandardBasicTypes.STRING.getName() );
		registerHibernateType( Types.CHAR, 1, StandardBasicTypes.STRING.getName() );
		
	}	

}
