package com.jad.dao.hibernate;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.ConstructorArgumentValues;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.jad.dao.AbstractJadEntityDao;
import com.jad.dao.enums.RespositoryType;

@Service
@Lazy(false)
public class HibernateRepositoryType implements BeanDefinitionRegistryPostProcessor{

	@Override
	public void postProcessBeanFactory(
			ConfigurableListableBeanFactory beanFactory) throws BeansException {
		
	}

	@Override
	public void postProcessBeanDefinitionRegistry(
			BeanDefinitionRegistry registry) throws BeansException {
		if(!registry.containsBeanDefinition(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME) 
				&& !registry.isAlias(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME)){
			RootBeanDefinition beanDefinition=new RootBeanDefinition(String.class);
			beanDefinition.setScope(BeanDefinition.SCOPE_SINGLETON);
			beanDefinition.setLazyInit(false);
			ConstructorArgumentValues constructorArgumentValues=new ConstructorArgumentValues();
			constructorArgumentValues.addIndexedArgumentValue(0, RespositoryType.HIBERNATE.getRespositoryType());
			beanDefinition.setConstructorArgumentValues(constructorArgumentValues);
			registry.registerBeanDefinition(AbstractJadEntityDao.REPOSITORY_TYPE_BEAN_NAME, beanDefinition);//注册 repositoryType
		}
		
	}


}
