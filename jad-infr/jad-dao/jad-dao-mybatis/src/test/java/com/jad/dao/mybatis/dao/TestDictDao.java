package com.jad.dao.mybatis.dao;

import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;
import com.jad.dao.mybatis.eo.TestDictEo;


@JadDao
public interface TestDictDao extends JadEntityDao<TestDictEo,String>{

}
