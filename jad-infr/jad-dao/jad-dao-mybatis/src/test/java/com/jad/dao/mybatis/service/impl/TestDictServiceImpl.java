package com.jad.dao.mybatis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.dao.mybatis.dao.TestDictDao;
import com.jad.dao.mybatis.eo.TestDictEo;
import com.jad.dao.mybatis.service.TestDictService;


@Service("testDictService")
@Transactional(readOnly = true)
public class TestDictServiceImpl implements TestDictService {
	@Autowired
	private TestDictDao testDictDao;
	
	public TestDictEo findById(String id){
		return testDictDao.findById(id);
	}
}
