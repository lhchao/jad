package com.jad.dao.mybatis.interceptor;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

/**
 * 拦截 select 语句，处理分页
 * 
 * @author hechuan
 * 
 */
@Intercepts({ @Signature(type = Executor.class, method = "query", args = {
		MappedStatement.class, Object.class, RowBounds.class,ResultHandler.class }) })
public class PageInterceptor extends JadAbstractInterceptor {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public Object intercept(Invocation invocation) throws Throwable {

		MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];

		Object parameter = invocation.getArgs()[1];
		
		MappedStatement newMs = parsePageParams(mappedStatement,parameter);//处理分页参数
		
		if(mappedStatement!=newMs){
	        invocation.getArgs()[0] = newMs;
		}
        
        return invocation.proceed();
	}
	
	
	
	
	

    
    
    
  
    
    

}
