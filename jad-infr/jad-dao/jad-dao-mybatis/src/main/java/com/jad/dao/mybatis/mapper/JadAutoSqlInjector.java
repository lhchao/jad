package com.jad.dao.mybatis.mapper;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.ibatis.builder.MapperBuilderAssistant;
import org.apache.ibatis.executor.keygen.KeyGenerator;
import org.apache.ibatis.executor.keygen.NoKeyGenerator;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.SqlCommandType;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.mapping.StatementType;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.session.Configuration;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.JadEntityDao;
import com.jad.dao.entity.EoMetaInfo;
import com.jad.dao.enums.DBType;
import com.jad.dao.mybatis.enums.SqlMethod;
import com.jad.dao.utils.EntityUtils;

public class JadAutoSqlInjector implements ISqlInjector {

	private static final Log logger = LogFactory.getLog(JadAutoSqlInjector.class);

	protected Configuration configuration;

	protected LanguageDriver languageDriver;

	protected MapperBuilderAssistant builderAssistant;

	protected DBType dbType = DBType.MYSQL;
	
	private static ISqlInjector sqlInjector;
	
	private static final CopyOnWriteArrayList<String> INJECT_CACHE = new CopyOnWriteArrayList<String>();

	
	public static ISqlInjector getSqlInjector() {
		if(sqlInjector==null){
			sqlInjector=new JadAutoSqlInjector();
		}
		return sqlInjector;
	}
	
	
	/**
	 * CRUD注入后给予标识 注入过后不再注入
	 *
	 * @param builderAssistant
	 * @param mapperClass
	 */
	public void inspectInject(MapperBuilderAssistant builderAssistant, Class<?> mapperClass) {
//		String className = mapperClass.toString();
		String className=mapperClass.getName();
//		Set<String> mapperRegistryCache = GlobalConfiguration.getMapperRegistryCache(builderAssistant.getConfiguration());
//		if (!mapperRegistryCache.contains(className)) {
		if (!INJECT_CACHE.contains(className) ) {
			inject(builderAssistant, mapperClass);
//			mapperRegistryCache.add(className);
			INJECT_CACHE.add(className);
		}
	}

	/**
	 * 注入单点 Sql
	 */
	public void inject(MapperBuilderAssistant builderAssistant, Class<?> mapperClass) {
		this.configuration = builderAssistant.getConfiguration();
		this.builderAssistant = builderAssistant;
		this.languageDriver = configuration.getDefaultScriptingLanuageInstance();
//		GlobalConfiguration globalCache = GlobalConfiguration.globalConfig(configuration);
//		this.dbType = globalCache.getDbType();
		/*
		 * 驼峰设置 PLUS 配置 > 原始配置
		 */
//		if (!globalCache.isDbColumnUnderline()) {
//			globalCache.setDbColumnUnderline(configuration.isMapUnderscoreToCamelCase());
//		}
//		Class<?> modelClass = extractModelClass(mapperClass);
//		if(modelClass==null){
//			logger.warn(String.format("无法从类%s中通过范型信息获得实体信息", mapperClass.getName()));
//			return;
//		}

//		TableInfo table = TableInfoHelper.initTableInfo(builderAssistant, modelClass);
		
//		EoMetaInfo table=EntityUtils.getEoInfo((Class<EntityObject>)modelClass);
		
		/**
		 * 没有指定主键，默认方法不能使用
		 */
//		if (table!=null && table.getKeyFieldInfo()!=null) {
			
			/*执行sql*/
			this.injectExecuteSql(mapperClass);
			
			this.injectFindBySql(mapperClass);
			
			this.injectFindPageBySql(mapperClass);
//			
//		} else {
//			/**
//			 * 警告
//			 */
//			logger.warn(String.format("%s ,类无法自动注入SqlSource.",modelClass.toString()));
//		}
	}

//	
	
	protected void injectFindPageBySql(Class<?> mapperClass){
		SqlMethod sqlMethod = SqlMethod.FIND_PAGE_BY_SQL;
		String sql = sqlMethod.getSql();
//		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, List.class);
		
		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, null);
		
		this.addFindBySqlMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, null);
		
	}
	
	
	protected void injectFindBySql(Class<?> mapperClass){
		SqlMethod sqlMethod = SqlMethod.FIND_BY_SQL;
		String sql = sqlMethod.getSql();
//		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, List.class);
		
		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, null);
		
		this.addFindBySqlMappedStatement(mapperClass, sqlMethod.getMethod(), sqlSource, null);
		
	}
	
	protected void injectExecuteSql(Class<?> mapperClass){
		SqlMethod sqlMethod = SqlMethod.EXECUTE_SQL;
		String sql = sqlMethod.getSql();
		SqlSource sqlSource = languageDriver.createSqlSource(configuration, sql, List.class);
		this.addUpdateMappedStatement(mapperClass,  sqlMethod.getMethod(), sqlSource);
	}

	

//	protected Class<?> extractModelClass(Class<?> mapperClass) {
//		Type[] types = mapperClass.getGenericInterfaces();
//		ParameterizedType target = null;
//		for (Type type : types) {
//			if (type instanceof ParameterizedType && JadEntityDao.class.isAssignableFrom(mapperClass)) {
//				target = (ParameterizedType) type;
//				break;
//			}
//		}
//		Type[] parameters = target.getActualTypeArguments();
//		Class<?> modelClass = (Class<?>) parameters[0];
//		return modelClass;
//	}


	
	
	public MappedStatement addFindBySqlMappedStatement(Class<?> mapperClass, String id, SqlSource sqlSource, Class<?> resultType) {
		/* 普通查询 */
		return this.addMappedStatement(mapperClass, id, sqlSource, SqlCommandType.SELECT, null, null, resultType,
				new NoKeyGenerator(), null, null);
	}
	



	
	public MappedStatement addUpdateMappedStatement(Class<?> mapperClass,  String id, SqlSource sqlSource) {
		return this.addMappedStatement(mapperClass, id, sqlSource, 
				SqlCommandType.UPDATE, List.class, null, Integer.class,
				new NoKeyGenerator(), null, null);
	}

	public MappedStatement addMappedStatement(Class<?> mapperClass, String id, SqlSource sqlSource,
			SqlCommandType sqlCommandType, Class<?> parameterClass, String resultMap, Class<?> resultType,
			KeyGenerator keyGenerator, String keyProperty, String keyColumn) {
		String statementName = mapperClass.getName() + "." + id;
		if (configuration.hasStatement(statementName)) {
			System.err.println("{" + statementName
					+ "} Has been loaded by XML or SqlProvider, ignoring the injection of the SQL.");
			return null;
		}
		/* 缓存逻辑处理 */
		boolean isSelect = false;
		if (sqlCommandType == SqlCommandType.SELECT) {
			isSelect = true;
		}
        return builderAssistant.addMappedStatement(id, sqlSource, StatementType.PREPARED, sqlCommandType, null, null, null,
				parameterClass, resultMap, resultType, null, !isSelect, isSelect, false, keyGenerator, keyProperty, keyColumn,
				configuration.getDatabaseId(), languageDriver, null);
	}

}
