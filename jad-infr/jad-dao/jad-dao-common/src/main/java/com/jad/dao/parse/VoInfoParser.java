package com.jad.dao.parse;

import com.jad.commons.vo.ValueObject;
import com.jad.dao.entity.VoMetaInfo;


/**
 * vo元信息解析
 * @author Administrator
 *
 */
public interface VoInfoParser {

	public <VO extends ValueObject> VoMetaInfo<VO> parseVo(Class<VO> clazz);
}
