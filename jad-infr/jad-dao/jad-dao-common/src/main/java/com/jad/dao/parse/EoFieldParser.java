package com.jad.dao.parse;

import java.lang.reflect.Field;

import com.jad.commons.vo.EntityObject;
import com.jad.dao.entity.EoFieldInfo;

/**
 * 实体字段解析
 * @author hechuan
 *
 */
public interface EoFieldParser  {
	
	/**
	 * 字段解析
	 * @param entityFieldInfo
	 * @param field
	 */
	<EO extends EntityObject> boolean parseEoField(EoFieldInfo<EO> entityFieldInfo,Field field);
	
}






