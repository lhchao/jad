package com.jad.commons.security.shiro;

import org.apache.shiro.authc.AuthenticationToken;

public class ApiToken implements AuthenticationToken{

	
	 /**
     * The username
     */
    private String username;

    /**
     */
    private String password;
    
    
	public ApiToken(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	@Override
	public Object getPrincipal() {
		return username;
	}

	@Override
	public Object getCredentials() {
		return password;
	}


}
