package com.jad.commons.security.shiro;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.AuthenticatingFilter;
import org.apache.shiro.web.util.WebUtils;

//${adminPath}/testsimple/innTest/**= api
//<entry key="api" value-ref="apiAuthenticationFilter"/>
//	<bean id="apiAuthenticationFilter" class="com.jad.commons.security.shiro.ApiAuthenticationFilter">  
//</bean>
//<bean id="apiAuthorizingRealm" class="com.jad.commons.security.shiro.ApiAuthorizingRealm">  
//</bean>
/*
 <!--多个realm 的集中管理  -->
    <bean id="defineModularRealmAuthenticator" class=" com.jscredit.zxypt.shiro.DefautModularRealm"> 
        <property name="definedRealms">    
            <map>    
                <entry key="loginRealm" value-ref="loginRealm" />    
                <entry key="userloginRealm" value-ref="userloginRealm" />    
            </map>   
        </property>  
        <property name="authenticationStrategy">    
            <bean class="org.apache.shiro.authc.pam.FirstSuccessfulStrategy" />    
        </property> 
    </bean>   
    
     
     	<!-- 定义Shiro安全管理配置 -->
	<bean id="securityManager" class="org.apache.shiro.web.mgt.DefaultWebSecurityManager">
		<property name="authenticator" ref="defineModularRealmAuthenticator" /> 
		<!-- 
		<property name="realm" ref="systemAuthorizingRealm" />
		 -->
		<property name="realms"  >
            <list>
               <bean id="sysRealm" class="com.jad.commons.security.shiro.SystemAuthorizingRealm" /> 
               <bean id="apiRealm" class="com.jad.commons.security.shiro.ApiAuthorizingRealm" /> 
            </list>
        </property>
        
		<property name="sessionManager" ref="sessionManager" />
		<!-- 
		<property name="cacheManager" ref="shiroCacheManager" />
		 -->
		<property name="rememberMeManager" ref="rememberMeManager" />
		
	</bean>
 
 
 */


/**
 * api认证
 * @author hechuan
 *
 */
public class ApiAuthenticationFilter extends AuthenticatingFilter {

	@Override
	protected AuthenticationToken createToken(ServletRequest request,
			ServletResponse response) throws Exception {
		String username = getUsername(request);
		String password = getPassword(request);
		return new ApiToken(username, password);
	}
	
    protected String getUsername(ServletRequest request) {
    	String username=WebUtils.getCleanParam(request, "username");
        return username == null ? "admin" : username.trim();
    }

    protected String getPassword(ServletRequest request) {
        String password=WebUtils.getCleanParam(request, "password");
        return password == null ? "admin" : password.trim();
    }

    

	@Override
	protected boolean onAccessDenied(ServletRequest request,
			ServletResponse response) throws Exception {
		// TODO Auto-generated method stub
//		return false;
		return true;
	}


}
