package com.jad.commons.security.shiro;

import java.util.List;

import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;

public class ApiAuthorizingRealm extends AuthorizingRealm {

	/**
	 * 证证回调
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(
			AuthenticationToken apiToken) throws AuthenticationException {
		// TODO Auto-generated method stub
		ApiToken token = (ApiToken) apiToken;
		
		String username = token.getPrincipal()+"";
//		TODO 从数据库中获得密码
		String dbPwd="admin";
		if(dbPwd.equals(token.getCredentials())){//认证通过
			
		}else {
			
		}
		ApiPrincipal paip=new ApiPrincipal();
		paip.setUsername(apiToken.getPrincipal()+"");
		return new SimpleAuthenticationInfo(paip,
				apiToken.getCredentials(), "apiRealmName");
	}

	/**
	 * 授权回调
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		ApiPrincipal apiPrincipal = (ApiPrincipal) principals.fromRealm(getName()).iterator().next();
		String username = apiPrincipal.getUsername();
		if (username != null) {
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			info.addStringPermission("testsimple:innTest:edit");
			return info;
//			// 查询用户授权信息
//			List<Permission> perList = permissionService.getShiro("O");
//			if (perList != null && perList.size() != 0) {
//				for (Permission permission : perList) {
//					info.addStringPermission(permission.getPmsnCode());
//				}
//				return info;
//			}
		}
		return null;
	}

}
