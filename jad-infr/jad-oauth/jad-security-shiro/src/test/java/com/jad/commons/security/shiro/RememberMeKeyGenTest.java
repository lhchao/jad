package com.jad.commons.security.shiro;

import javax.crypto.KeyGenerator;


/**
 * 生成RememberMe模块所需的128位AES密钥
 * @author hechuan
 *
 */
public class RememberMeKeyGenTest {


	
	
	public static void main(String[] args)throws Exception {
		
		KeyGenerator gen=KeyGenerator.getInstance("AES");
    	gen.init(128);
    	
    	byte[]bt=gen.generateKey().getEncoded();
    	String str=org.apache.shiro.codec.Base64.encodeToString(bt);
    	System.out.println("key:"+str);

	}

}
