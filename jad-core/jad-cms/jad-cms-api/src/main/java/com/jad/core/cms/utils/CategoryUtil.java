package com.jad.core.cms.utils;

import org.apache.commons.lang3.StringUtils;

import com.jad.core.cms.vo.CategoryVo;

public class CategoryUtil {
	

	private static final String ROOT_ID = "1";
	
	public static boolean isRoot(String id){
		return id != null && id.equals(ROOT_ID);
	}
	
	public static String getRootId(){
		return ROOT_ID;
	}
	
	
  	/**
     * 获得栏目动态URL地址
   	 * @param category
   	 * @return url
   	 */
    public static String getUrlDynamic(CategoryVo category,String contextPath,String frontPath,String urlSuffix) {
        if(StringUtils.isNotBlank(category.getHref())){
            if(!category.getHref().contains("://")){
//                return context.getContextPath()+Global.getFrontPath()+category.getHref();
            	return contextPath+frontPath+category.getHref();
            }else{
                return category.getHref();
            }
        }
        StringBuilder str = new StringBuilder();
        str.append(contextPath).append(frontPath);
        str.append("/list-").append(category.getId()).append(urlSuffix);
        return str.toString();
    }
    
    
}
