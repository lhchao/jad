package com.jad.core.cms.qo;

import com.jad.commons.annotation.CURD;
import com.jad.commons.annotation.QueryConf;
import com.jad.commons.qo.BaseQo;

public class ArticleQo extends BaseQo<String> {
	
	private static final long serialVersionUID = 1L;
	
	@CURD(label="分类编号",queryConf=@QueryConf(property="category.id"))
	private String categoryId;// 分类编号
	
	
	@CURD(label="标题")
	private String title;	// 标题
	
//	private Date beginDate;	// 开始时间
//	private Date endDate;	// 结束时间

	public ArticleQo() {
	}

	public ArticleQo(String id) {
		super(id);
	}
	
	

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
