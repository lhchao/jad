/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.vo;

import java.util.Date;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jad.commons.annotation.CURD;
import com.jad.commons.vo.BaseVo;
import com.jad.core.sys.vo.UserVo;

/**
 * 留言Entity
 * @author ThinkGem
 * @version 2013-05-15
 */
public class GuestbookVo extends BaseVo<String> {
	
	private static final long serialVersionUID = 1L;
	
	@CURD(label="留言分类")
	private String type; 	// 留言分类（咨询、建议、投诉、其它）
	
	@CURD(label="留言内容")
	private String content; // 留言内容
	
	@CURD(label="姓名")
	private String name; 	// 姓名
	
	@CURD(label="邮箱")
	private String email; 	// 邮箱
	
	@CURD(label="电话")
	private String phone; 	// 电话
	
	@CURD(label="单位")
	private String workunit;// 单位
	
	@CURD(label="留言IP")
	private String ip; 		// 留言IP
	
	@CURD(label="回复时间")
	private Date reDate;	// 回复时间
	
	
	private UserVo reUser; 		// 回复人
	
	@CURD(label="回复内容")
	private String reContent;// 回复内容

	public GuestbookVo() {
	}

	public GuestbookVo(String id){
		this();
		this.id = id;
	}
	
	@Length(min=1, max=100)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Length(min=1, max=2000)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Length(min=1, max=100)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Email @Length(min=0, max=100)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Length(min=0, max=100)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Length(min=0, max=100)
	public String getWorkunit() {
		return workunit;
	}

	public void setWorkunit(String workunit) {
		this.workunit = workunit;
	}

	@Length(min=1, max=100)
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getReContent() {
		return reContent;
	}

	public void setReContent(String reContent) {
		this.reContent = reContent;
	}

	public Date getReDate() {
		return reDate;
	}

	public void setReDate(Date reDate) {
		this.reDate = reDate;
	}

	@JsonIgnore
	public UserVo getReUser() {
		return reUser;
	}

	public void setReUser(UserVo reUser) {
		this.reUser = reUser;
	}


	
}


