/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service;

import com.jad.commons.service.CurdService;
import com.jad.core.cms.vo.LinkVo;

/**
 * 链接Service
 * @author ThinkGem
 * @version 2013-01-15
 */
public interface LinkService extends CurdService<LinkVo,String> {
	
//	public Page<LinkVo> findPage(Page<LinkVo> page, LinkQo link, boolean isDataScopeFilter) ;
	
//	public void delete(LinkVo link, Boolean isRe) ;
	
	/**
	 * 通过编号获取内容标题
	 */
//	public List<Object[]> findByIds(String ids) ;

}
