/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service;

import java.util.List;

import com.jad.commons.service.CurdService;
import com.jad.commons.vo.Page;
import com.jad.core.cms.qo.ArticleQo;
import com.jad.core.cms.vo.ArticleVo;

/**
 * 文章Service
 * @author ThinkGem
 * @version 2013-05-15
 */
public interface ArticleService extends CurdService<ArticleVo,String> {

	
//	public Page<ArticleVo> findPage(Page<ArticleVo> page, ArticleQo articleQo, boolean isDataScopeFilter) ;

	
//	public void delete(ArticleVo article, Boolean isRe) ;
	
	/**
	 * 通过编号获取内容标题
	 * @return new Object[]{栏目Id,文章Id,文章标题}
	 */
//	public List<Object[]> findByIds(String ids) ;
	
	/**
	 * 点击数加一
	 */
	public void updateHitsAddOne(String id) ;
	
//	/**
//	 * 更新索引
//	 */
//	public void createIndex();
	
//	/**
//	 * 全文检索
//	 */
//	//FIXME 暂不提供检索功能
//	public Page<ArticleVo> search(Page<ArticleVo> page, String q, String categoryId, String beginDate, String endDate);
	
}
