/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.core.cms.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.commons.utils.DateUtils;
import com.jad.core.cms.dao.ArticleDao;
import com.jad.core.cms.qo.CategoryQo;
import com.jad.core.cms.service.StatsService;
import com.jad.core.cms.utils.SiteUtil;
import com.jad.core.cms.vo.CategoryVo;

/**
 * 统计Service
 * @author ThinkGem
 * @version 2013-05-21
 */
@Service("statsService")
@Transactional(readOnly = true)
public class StatsServiceImpl implements StatsService {

	@Autowired
	private ArticleDao articleDao;
	
	
	public List<CategoryVo> article(Map<String, Object> paramMap) {
		CategoryQo category = new CategoryQo();
		
		category.setSiteId(SiteUtil.getCurrentSiteId());
		
		Date beginDate = DateUtils.parseDate(paramMap.get("beginDate"));
		if (beginDate == null){
			beginDate = DateUtils.setDays(new Date(), 1);
			paramMap.put("beginDate", DateUtils.formatDate(beginDate, "yyyy-MM-dd"));
		}
		category.setUpdateDateBegin(beginDate);
		Date endDate = DateUtils.parseDate(paramMap.get("endDate"));
		if (endDate == null){
			endDate = DateUtils.addDays(DateUtils.addMonths(beginDate, 1), -1);
			paramMap.put("endDate", DateUtils.formatDate(endDate, "yyyy-MM-dd"));
		}
		category.setUpdateDateEnd(endDate);
		
		String categoryId = (String)paramMap.get("categoryId");
		if (categoryId != null && !("".equals(categoryId))){
			category.setId(categoryId);
			category.setParentIds(categoryId);
		}
		
		String officeId = (String)(paramMap.get("officeId"));
//		OfficeVo office = new OfficeVo();
//		if (officeId != null && !("".equals(officeId))){
//			office.setId(officeId);
//			category.setOffice(office);
//		}else{
//			category.setOffice(office);
//		}
		
		List<CategoryVo> list = articleDao.findStats(category);
		return list;
	}

}
