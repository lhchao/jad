/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.cms.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.jad.commons.annotation.ApiCurdType;
import com.jad.commons.context.Global;
import com.jad.commons.enums.CurdType;
import com.jad.commons.security.shiro.SecurityHelper;
import com.jad.commons.util.TreeUtil;
import com.jad.commons.utils.StringUtils;
import com.jad.commons.web.api.Result;
import com.jad.core.cms.qo.CategoryQo;
import com.jad.core.cms.service.CategoryService;
import com.jad.core.cms.service.SiteService;
import com.jad.core.cms.utils.CategoryUtil;
import com.jad.core.cms.utils.FileTplUtil;
import com.jad.core.cms.vo.CategoryVo;
import com.jad.web.cms.util.SiteHelper;
import com.jad.web.cms.util.TplApiUtils;
import com.jad.web.mvc.BaseController;

/**
 * 栏目Controller
 * @author ThinkGem
 * @version 2013-4-21
 */
@Controller
@RequestMapping(value = "${adminPath}/cms/category")
@Api(description = "栏目管理")
public class CategoryController extends BaseController {

	@Autowired
	private CategoryService categoryService;
    @Autowired
   	private SiteService siteService;
    
//    @Autowired
//    private ServletContext context;
    
//	@ModelAttribute("category")
//	public CategoryVo get(@RequestParam(required=false) String id) {
//		if (StringUtils.isNotBlank(id)){
//			return categoryService.findById(id);
//		}else{
//			return new CategoryVo();
//		}
//	}
    
    
    /**
	 * 查看单条记录
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:category:view")
	@RequestMapping(value = {"find"})
	@ResponseBody
	@ApiOperation(value = "查看栏目信息",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_ONE)
	public Result<CategoryVo> find(@RequestParam(required=true) String id){
		return Result.newSuccResult(categoryService.findById(id));
	}
	
	/**
	 * 查询列表
	 * @param userQo
	 * @param pageQo
	 * @return
	 */
	@RequiresPermissions("sys:category:view")
	@ResponseBody
	@RequestMapping(value = {"findList"})
	@ApiOperation(value = "查询栏目列表",httpMethod = "GET")
	@ApiCurdType(CurdType.FIND_LIST)
	public Result<List<CategoryVo>> findList(CategoryQo categoryQo){
		List<CategoryVo> list = new ArrayList<CategoryVo>();
		List<CategoryVo> sourcelist = categoryService.findList(new CategoryQo());
		TreeUtil.sortList(list, sourcelist, CategoryUtil.getRootId());
		return Result.newSuccResult(list);
	}
	
	/**
	 * 修改
	 * @param drticleVo
	 * @return
	 */
	@RequiresPermissions("sys:category:edit")
	@RequestMapping(value = "update")
	@ResponseBody
	@ApiOperation(value = "修改栏目信息",httpMethod = "POST")
	@ApiCurdType(CurdType.UPDATE)
	public Result<?> update(CategoryVo drticleVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, drticleVo)){
			return result;
		}
		if(StringUtils.isBlank(drticleVo.getId())){
			result.setParamFail("修改失败,末知的id");
			return result;
		}
		preUpdate(drticleVo,SecurityHelper.getCurrentUserId());
		categoryService.update(drticleVo);
		return result;
	}
	
	/**
	 * 新增
	 * @param userVo
	 * @return
	 */
	@RequiresPermissions("sys:category:edit")
	@RequestMapping(value = "add")
	@ResponseBody
	@ApiOperation(value = "新增栏目信息",httpMethod = "POST")
	@ApiCurdType(CurdType.ADD)
	public Result<?> add(CategoryVo drticleVo){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		if (!beanValidator(result, drticleVo)){
			return result;
		}
		preInsert(drticleVo,SecurityHelper.getCurrentUserId());
		categoryService.add(drticleVo);
		return result;
	}
	
	/**
	 * 删除
	 * @param id
	 * @return
	 */
	@RequiresPermissions("sys:category:edit")
	@RequestMapping(value = "del")
	@ResponseBody
	@ApiOperation(value = "删除栏目信息",httpMethod = "POST")
	@ApiCurdType(CurdType.DELETE)
	public Result<?> delete(@RequestParam(required=true)String id){
		Result<?> result = Result.newSuccResult();
		if(Global.isDemoMode()){
			result.setBusiFail("演示模式，不允许操作！");
			return result;
		}
		CategoryVo delVo = new CategoryVo();
		delVo.setId(id);
		preUpdate(delVo,SecurityHelper.getCurrentUserId());
		categoryService.delete(delVo);
		return result;
	}
	

	@RequiresPermissions("cms:category:view")
	@RequestMapping(value = {"list", ""})
	public String list(Model model) {
		List<CategoryVo> list = new ArrayList();
		List<CategoryVo> sourcelist = categoryService.findList(new CategoryQo());
		TreeUtil.sortList(list, sourcelist, CategoryUtil.getRootId());
        model.addAttribute("list", list);
		return "modules/cms/categoryList";
	}

	@RequiresPermissions("cms:category:view")
	@RequestMapping(value = "form")
	public String form(CategoryQo categoryQo, Model model,HttpServletRequest request) {
		
		CategoryVo categoryVo = new CategoryVo();
		
		if(StringUtils.isNotBlankStr(categoryQo.getId())){ //修改
			
			categoryVo = categoryService.findById(categoryQo.getId());
			
		}else if(StringUtils.isNotBlank(categoryQo.getParentId())){//添加下级
			
			categoryVo.setParent(categoryService.findById(categoryQo.getParentId()));
			
		}else{ //添加新的
			String parentId = CategoryUtil.getRootId();
			categoryVo.setParent(categoryService.findById(parentId));
		}
		
		
//		if (categoryVo.getParent()==null||categoryVo.getParent().getId()==null){
//			categoryVo.setParent(new CategoryVo(CategoryUtil.getRootId()));
//		}
//		CategoryVo parent = categoryService.findById(categoryVo.getParent().getId());
//		categoryVo.setParent(parent);
		
//		if (category.getOffice()==null||category.getOffice().getId()==null){
//			category.setOffice(parent.getOffice());
//		}
		return toForm(categoryVo,model,request);
	}
	
	private String toForm(CategoryVo categoryVo, Model model,HttpServletRequest request){
		model.addAttribute("listViewList",getTplContent(CategoryVo.DEFAULT_TEMPLATE,request));
        model.addAttribute("category_DEFAULT_TEMPLATE",CategoryVo.DEFAULT_TEMPLATE);
        model.addAttribute("contentViewList",getTplContent(CategoryVo.DEFAULT_TEMPLATE,request));
        model.addAttribute("article_DEFAULT_TEMPLATE",CategoryVo.DEFAULT_TEMPLATE);
//		model.addAttribute("office", category.getOffice());
		model.addAttribute("categoryVo", categoryVo);
		return "modules/cms/categoryForm";
	}
	
	
	@RequiresPermissions("cms:category:edit")
	@RequestMapping(value = "save")
	public String save(CategoryVo categoryVo, Model model, RedirectAttributes redirectAttributes,HttpServletRequest request) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/cms/category/";
		}
		if (!beanValidator(model, categoryVo)){
			return toForm(categoryVo, model,request);
		}
		if(StringUtils.isBlankStr(categoryVo.getModule())){
			categoryVo.setModule("");
		}
		if(StringUtils.isNotBlank(categoryVo.getId())){
			preUpdate(categoryVo, SecurityHelper.getCurrentUserId());
			categoryService.update(categoryVo);
		}else{
			preInsert(categoryVo, SecurityHelper.getCurrentUserId());
			categoryService.add(categoryVo);
		}
		
		
		addMessage(redirectAttributes, "保存栏目'" + categoryVo.getName() + "'成功");
		return "redirect:" + adminPath + "/cms/category/";
	}
	
	@RequiresPermissions("cms:category:edit")
	@RequestMapping(value = "delete")
	public String delete(@RequestParam(required=true)String id,  RedirectAttributes redirectAttributes) {
		if(Global.isDemoMode()){
			addMessage(redirectAttributes, "演示模式，不允许操作！");
			return "redirect:" + adminPath + "/cms/category/";
		}
		if (CategoryUtil.isRoot(id)){
			addMessage(redirectAttributes, "删除栏目失败, 不允许删除顶级栏目或编号为空");
		}else{
			
			CategoryVo delVo = new CategoryVo();
			delVo.setId(id);
			preUpdate(delVo,SecurityHelper.getCurrentUserId());
			categoryService.delete(delVo);
			
			addMessage(redirectAttributes, "删除栏目成功");
		}
		return "redirect:" + adminPath + "/cms/category/";
	}

	/**
	 * 批量修改栏目排序
	 */
	@RequiresPermissions("cms:category:edit")
	@RequestMapping(value = "updateSort")
	public String updateSort(@RequestParam(required=true)String[] ids, @RequestParam(required=true)Integer[] sorts, RedirectAttributes redirectAttributes) {
    	int len = ids.length;
//    	CategoryVo[] entitys = new CategoryVo[len];
    	for (int i = 0; i < len; i++) {
//    		entitys[i] = categoryService.findById(ids[i]);
//    		entitys[i].setSort(sorts[i]);
    		categoryService.updateSort(ids[i], sorts[i]);
//    		if(StringUtils.isNotBlank(entitys[i].getId())){
//    			categoryService.update(entitys[i]);
//    		}else{
//    			categoryService.add(entitys[i]);
//    		}
//    		categoryService.save(entitys[i]);
    	}
    	addMessage(redirectAttributes, "保存栏目排序成功!");
		return "redirect:" + adminPath + "/cms/category/";
	}
	
	@RequiresPermissions("user")
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(String module, @RequestParam(required=false) String extId, HttpServletResponse response) {
//		response.setContentType("application/json; charset=UTF-8");
		List<Map<String, Object>> mapList = new ArrayList();
//		List<CategoryVo> list = categoryService.findByUser(true, module);
		List<CategoryVo> list = categoryService.findList(new CategoryQo());
		for (int i=0; i<list.size(); i++){
			CategoryVo e = list.get(i);
			if (extId == null || (extId!=null && !extId.equals(e.getId()) && e.getParentIds().indexOf(","+extId+",")==-1)){
				Map<String, Object> map = new HashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParent()!=null?e.getParent().getId():0);
				map.put("name", e.getName());
				map.put("module", e.getModule());
				mapList.add(map);
			}
		}
		return mapList;
	}

    private List<String> getTplContent(String prefix,HttpServletRequest request) {
    	String path=siteService.findById(SiteHelper.getCurrentSiteId()).getSolutionPath();
    	
    	
   		List<String> tplList = FileTplUtil.getNameListByPrefix(request.getSession().getServletContext().getRealPath(path),
   				request.getSession().getServletContext().getRealPath(""));
   		tplList = TplApiUtils.tplTrim(tplList, prefix, "");
   		return tplList;
   	}
}
