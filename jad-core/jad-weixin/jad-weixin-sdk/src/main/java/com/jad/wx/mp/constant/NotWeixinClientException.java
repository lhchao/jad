package com.jad.wx.mp.constant;

/**
 * 客户端不是微信浏览器
 * @author hechuan
 *
 */
public class NotWeixinClientException extends RuntimeException {

	public NotWeixinClientException() {
		// TODO Auto-generated constructor stub
	}

	public NotWeixinClientException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public NotWeixinClientException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public NotWeixinClientException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public NotWeixinClientException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
