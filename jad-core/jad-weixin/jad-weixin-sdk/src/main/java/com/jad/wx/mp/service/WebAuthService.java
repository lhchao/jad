package com.jad.wx.mp.service;

import com.jad.wx.mp.constant.WeixinMpException;
import com.jad.wx.mp.resp.CommonResp;
import com.jad.wx.mp.resp.GetUserInfoResp;
import com.jad.wx.mp.resp.RefreshTokenResp;
import com.jad.wx.mp.resp.WebAuthorizeResp;

/**
 * 网页授权服务
 * @author hechuan
 *
 */
public interface WebAuthService {
	
	
	/**
	 * token验证
	 * @param openid //用户的唯一标识    
	 * @param access_token //网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
	 * @return
	 * @throws WeixinMpException
	 */
	CommonResp checkToken(String openid, String access_token)throws WeixinMpException ;
	
	/**
	 * 刷新token
	 * 主要用于刷新微信用户web授权
	 * @param appid
	 * @param refresh_token
	 * @return
	 * @throws WeixinMpException
	 */
	RefreshTokenResp refreshToken(String appid,String refresh_token)throws WeixinMpException;
	
	/**
	 * 跟据code获得网页授权
	 * @param appid
	 * @param secret
	 * @param code
	 * @return
	 * @throws WeixinMpException
	 */
	WebAuthorizeResp getWebAuthorize(String appid,String secret,String code )throws WeixinMpException;
	
	/**
	 * 拉取用户信息(需scope为 snsapi_userinfo)
	 * @param openid 用户的唯一标识
	 * @param access_token 网页授权接口调用凭证,注意：此access_token与基础支持的access_token不同
	 * @return
	 * @throws WeixinMpException
	 */
	GetUserInfoResp getWxmpUserInfo( String openid,String access_token) throws WeixinMpException;
	
	/**
	 * 生成授予仅跳转连接
	 * 用于引导用户进入授权界面
	 * @param appid 是	公众号的唯一标识
	 * @param secret 是	公众号的appsecret
	 * @param redirect_uri 是	授权后重定向的回调链接地址，请使用urlEncode对链接进行处理
	 * @param scope 是	应用授权作用域，snsapi_base （不弹出授权页面，直接跳转，只能获取用户openid），
	 * 				snsapi_userinfo （弹出授权页面，可通过openid拿到昵称、性别、所在地。并且，即使在未关注的情况下，只要用户授权，也能获取其信息）
	 * @param state 否	重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
	 * @return 如果用户同意授权，页面将跳转至 redirect_uri/?code=CODE&state=STATE。
	 * @throws WeixinMpException
	 */
	String genAuthorizeUrl(String appid, String redirect_uri, String scope, String state) throws WeixinMpException ;
}
