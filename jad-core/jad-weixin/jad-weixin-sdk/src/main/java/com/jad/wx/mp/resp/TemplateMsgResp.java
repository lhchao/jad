package com.jad.wx.mp.resp;


public class TemplateMsgResp extends CommonResp {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String msgid;// "msgid":200228332

	public String getMsgid() {
		return msgid;
	}

	public void setMsgid(String msgid) {
		this.msgid = msgid;
	}

}
