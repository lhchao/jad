package com.jad.wx.mp.req;

import java.io.Serializable;

/**
 * 客服接口请求
 * @author hechuan
 *
 */
public class CustomReq implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String kf_account;//客服帐号
	private String nickname;//名称
	private String password;//密码
	
	public String getKf_account() {
		return kf_account;
	}
	public void setKf_account(String kf_account) {
		this.kf_account = kf_account;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	

}
