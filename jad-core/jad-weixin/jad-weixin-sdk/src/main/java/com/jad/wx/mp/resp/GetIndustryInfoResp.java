package com.jad.wx.mp.resp;

/**
 * 获取行业信息
 * @author hechuan
 *
 */
public class GetIndustryInfoResp  extends CommonResp {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String access_token;//是	接口调用凭证
	private GetIndustryItemResp primary_industry;//是	帐号设置的主营行业
	private GetIndustryItemResp secondary_industry;//是	帐号设置的副营行业
	
	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}
	public GetIndustryItemResp getPrimary_industry() {
		return primary_industry;
	}
	public void setPrimary_industry(GetIndustryItemResp primary_industry) {
		this.primary_industry = primary_industry;
	}
	public GetIndustryItemResp getSecondary_industry() {
		return secondary_industry;
	}
	public void setSecondary_industry(GetIndustryItemResp secondary_industry) {
		this.secondary_industry = secondary_industry;
	}
	

}
