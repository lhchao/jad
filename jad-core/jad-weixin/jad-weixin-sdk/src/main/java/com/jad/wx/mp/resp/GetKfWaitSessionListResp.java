package com.jad.wx.mp.resp;

import java.util.List;

public class GetKfWaitSessionListResp extends CommonResp{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int count;//	未接入会话数量
	
	private List<GetKfWaitSessionListItemResp>waitcaselist;

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public List<GetKfWaitSessionListItemResp> getWaitcaselist() {
		return waitcaselist;
	}

	public void setWaitcaselist(List<GetKfWaitSessionListItemResp> waitcaselist) {
		this.waitcaselist = waitcaselist;
	}

	
	

}
