package com.jad.bpm.service.leave;

import java.util.Date;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.runtime.ProcessInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jad.bpm.dao.leave.LeaveDao;
import com.jad.bpm.dto.leave.Leave;
import com.jad.bpm.entity.LeaveEo;
import com.jad.commons.service.AbstractPreService;
import com.jad.dao.utils.EntityUtils;

import org.activiti.engine.delegate.TaskListener;

/**
 * 销假后处理器
 * @author liuj
 */
@Service
@Transactional
public class LeaveReportProcessorImpl extends AbstractPreService implements LeaveReportProcessor ,TaskListener{

	private static final long serialVersionUID = 1L;

	@Autowired
	private LeaveDao leaveDao;
	@Autowired
	private RuntimeService runtimeService;
	
	/**
	 * 销假完成后执行，保存实际开始和结束时间
	 */
	public void notify(DelegateTask delegateTask) {
		String processInstanceId = delegateTask.getProcessInstanceId();
		ProcessInstance processInstance = runtimeService.createProcessInstanceQuery().processInstanceId(processInstanceId).singleResult();
		Leave leave = new Leave(processInstance.getBusinessKey());
		leave.setRealityStartTime((Date) delegateTask.getVariable("realityStartTime"));
		leave.setRealityEndTime((Date) delegateTask.getVariable("realityEndTime"));
//		leave.preUpdate();
//		leaveDao.updateRealityTime(leave);
		super.preUpdate(leave);
		leaveDao.updateRealityTime(EntityUtils.copyToEo(leave, LeaveEo.class));
		
	}

}
