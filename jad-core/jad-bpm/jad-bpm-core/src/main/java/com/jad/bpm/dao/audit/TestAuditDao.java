/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.bpm.dao.audit;

import com.jad.bpm.entity.TestAuditEo;
import com.jad.bpm.vo.TestAuditVo;
import com.jad.dao.JadEntityDao;
import com.jad.dao.annotation.JadDao;

/**
 * 审批DAO接口
 * @author thinkgem
 * @version 2014-05-16
 */
@JadDao
public interface TestAuditDao extends JadEntityDao<TestAuditEo,String> {

	public TestAuditEo getByProcInsId(String procInsId);
	
	public int updateInsId(TestAuditVo testAudit);
	
	public int updateHrText(TestAuditVo testAudit);
	
	public int updateLeadText(TestAuditVo testAudit);
	
	public int updateMainLeadText(TestAuditVo testAudit);
	
}
