/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.bpm.service;

import java.io.InputStream;

import org.springframework.web.multipart.MultipartFile;

import com.jad.bpm.service.ActProcessService;

/**
 * 流程定义相关Controller
 * @author ThinkGem
 * @version 2013-11-03
 */
public interface ActProcessManager extends ActProcessService {
	
	/**
	 * 流程定义列表
	 */
//	public Page<Object[]> processList(Page<Object[]> page, String category) ;

	/**
	 * 流程定义列表
	 */
//	public Page<ProcessInstance> runningList(Page<ProcessInstance> page, String procInsId, String procDefKey) ;
	
	/**
	 * 读取资源，通过部署ID
	 * @param processDefinitionId  流程定义ID
	 * @param processInstanceId 流程实例ID
	 * @param resourceType 资源类型(xml|image)
	 */
//	public InputStream resourceRead(String procDefId, String proInsId, String resType) throws Exception ;
	
	/**
	 * 部署流程 - 保存
	 * @param file
	 * @return
	 */
	public String deploy(String exportDir, String category, MultipartFile file) ;
	
	/**
	 * 设置流程分类
	 */
//	public void updateCategory(String procDefId, String category) ;

	/**
	 * 挂起、激活流程实例
	 */
//	public String updateState(String state, String procDefId) ;
	
	/**
	 * 将部署的流程转换为模型
	 * @param procDefId
	 * @throws UnsupportedEncodingException
	 * @throws XMLStreamException
	 */
//	public org.activiti.engine.repository.Model convertToModel(String procDefId) throws UnsupportedEncodingException, XMLStreamException ;
	
//	/**
//	 * 导出图片文件到硬盘
//	 */
//	public List<String> exportDiagrams(String exportDir) throws IOException ;
//
//	/**
//	 * 删除部署的流程，级联删除流程实例
//	 * @param deploymentId 流程部署ID
//	 */
//	public void deleteDeployment(String deploymentId) ;
//	
//	/**
//	 * 删除部署的流程实例
//	 * @param procInsId 流程实例ID
//	 * @param deleteReason 删除原因，可为空
//	 */
//	public void deleteProcIns(String procInsId, String deleteReason) ;
	
}
