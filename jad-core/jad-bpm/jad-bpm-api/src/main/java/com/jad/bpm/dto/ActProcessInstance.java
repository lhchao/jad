package com.jad.bpm.dto;

import java.io.Serializable;
import java.util.Map;

/**
 * 流程实例
 * 
 * @author Administrator
 *
 */
public class ActProcessInstance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The unique identifier of the execution.
	 */
	private String id;

	/**
	 * Indicates if the execution is suspended.
	 */
	private boolean suspended;
	/**
	 * Indicates if the execution is ended.
	 */
	private boolean ended;
	/**
	 * Returns the id of the activity where the execution currently is at.
	 * Returns null if the execution is not a 'leaf' execution (eg concurrent
	 * parent).
	 */
	private String activityId;

	/**
	 * Id of the root of the execution tree representing the process instance.
	 * It is the same as {@link #getId()} if this execution is the process
	 * instance.
	 */
	private String processInstanceId;

	/**
	 * Gets the id of the parent of this execution. If null, the execution
	 * represents a process-instance.
	 */
	private String parentId;

	/**
	 * The tenant identifier of this process instance
	 */
	private String tenantId;

	/**
	 * The id of the process definition of the process instance.
	 */
	private String processDefinitionId;

	/**
	 * The business key of this process instance.
	 */
	private String businessKey;

	/**
	 * Returns the process variables if requested in the process instance query
	 */
	private Map<String, Object> processVariables;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isSuspended() {
		return suspended;
	}

	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}

	public boolean isEnded() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public Map<String, Object> getProcessVariables() {
		return processVariables;
	}

	public void setProcessVariables(Map<String, Object> processVariables) {
		this.processVariables = processVariables;
	}

}
