package com.jad.bpm.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

public class ActDelegateTask implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** DB id of the task. */
	  private String id;
	  
	  /** Name or title of the task. */
	  private String name;
	  
	  

	  /** Free text description of the task. */
	  private String description;
	  
	  /** indication of how important/urgent this task is with a number between 
	   * 0 and 100 where higher values mean a higher priority and lower values mean 
	   * lower priority: [0..19] lowest, [20..39] low, [40..59] normal, [60..79] high 
	   * [80..100] highest */
	  private int priority;
	  
	  
	  /** Reference to the process instance or null if it is not related to a process instance. */
	  private String processInstanceId;
	  
	  /** Reference to the path of execution or null if it is not related to a process instance. */
	  private String executionId;
	  
	  /** Reference to the process definition or null if it is not related to a process. */
	  private String processDefinitionId;

	  /** The date/time when this task was created */
	  private Date createTime;
	  
	  /** The id of the activity in the process defining this task or null if this is not related to a process */
	  private String taskDefinitionKey;
	  
	  /** Returns the execution currently at the task. */
	  private ActDelegateExecution execution;
	  
	  /** Returns the event name which triggered the task listener to fire for this task. */
	  private String eventName;
	  

	  /** The {@link User.getId() userId} of the person responsible for this task. */
	  private String owner;
	  
	  
	  /** The {@link User.getId() userId} of the person to which this task is delegated. */
	  private String assignee;
	  
	  
	  /** Due date of the task. */
	  private Date dueDate;
	  
	  
	  /** The category of the task. This is an optional field and allows to 'tag' tasks as belonging to a certain category. */
	  private String category;
		
	  
	  
	  private Set<ActIdentityLink> candidates;



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public int getPriority() {
		return priority;
	}



	public void setPriority(int priority) {
		this.priority = priority;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}



	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}



	public String getExecutionId() {
		return executionId;
	}



	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}



	public String getProcessDefinitionId() {
		return processDefinitionId;
	}



	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}



	public Date getCreateTime() {
		return createTime;
	}



	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}



	public String getTaskDefinitionKey() {
		return taskDefinitionKey;
	}



	public void setTaskDefinitionKey(String taskDefinitionKey) {
		this.taskDefinitionKey = taskDefinitionKey;
	}



	public ActDelegateExecution getExecution() {
		return execution;
	}



	public void setExecution(ActDelegateExecution execution) {
		this.execution = execution;
	}



	public String getEventName() {
		return eventName;
	}



	public void setEventName(String eventName) {
		this.eventName = eventName;
	}



	public String getOwner() {
		return owner;
	}



	public void setOwner(String owner) {
		this.owner = owner;
	}



	public String getAssignee() {
		return assignee;
	}



	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}



	public Date getDueDate() {
		return dueDate;
	}



	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}



	public String getCategory() {
		return category;
	}



	public void setCategory(String category) {
		this.category = category;
	}



	public Set<ActIdentityLink> getCandidates() {
		return candidates;
	}



	public void setCandidates(Set<ActIdentityLink> candidates) {
		this.candidates = candidates;
	}
	  
	  

}
