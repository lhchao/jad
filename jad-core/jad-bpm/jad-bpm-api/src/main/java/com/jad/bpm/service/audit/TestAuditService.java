/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.bpm.service.audit;

import com.jad.bpm.vo.TestAuditVo;
import com.jad.commons.service.CurdService;

/**
 * 审批Service
 * @author thinkgem
 * @version 2014-05-16
 */
public interface TestAuditService extends CurdService<TestAuditVo,String> {
	
	public TestAuditVo getByProcInsId(String procInsId) ;
	
	/**
	 * 审核审批保存
	 * @param testAudit
	 */
	public void auditSave(TestAuditVo testAudit) ;
	
}
