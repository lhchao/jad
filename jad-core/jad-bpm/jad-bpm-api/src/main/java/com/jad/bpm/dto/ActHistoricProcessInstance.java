package com.jad.bpm.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class ActHistoricProcessInstance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

	/** The user provided unique reference to this process instance. */
	private String businessKey;

	/** The process definition reference. */
	private String processDefinitionId;

	/** The time the process was started. */
	private Date startTime;

	/** The time the process was ended. */
	private Date endTime;

	/**
	 * The difference between {@link #getEndTime()} and {@link #getStartTime()}
	 * .
	 */
	private Long durationInMillis;

	/**
	 * Reference to the activity in which this process instance ended. Note that
	 * a process instance can have multiple end events, in this case it might
	 * not be deterministic which activity id will be referenced here. Use a
	 * {@link HistoricActivityInstanceQuery} instead to query for end events of
	 * the process instance (use the activityTYpe attribute)
	 * */
	private String endActivityId;

	/**
	 * The authenticated user that started this process instance.
	 * 
	 * @see IdentityService#setAuthenticatedUserId(String)
	 */
	private String startUserId;

	/** The start activity. */
	private String startActivityId;

	/** Obtains the reason for the process instance's deletion. */
	private String deleteReason;

	/**
	 * The process instance id of a potential super process instance or null if
	 * no super process instance exists
	 */
	private String superProcessInstanceId;

	/**
	 * The tenant identifier for the process instance.
	 */
	private String tenantId;

	/** Returns the process variables if requested in the process instance query */
	private Map<String, Object> processVariables;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getDurationInMillis() {
		return durationInMillis;
	}

	public void setDurationInMillis(Long durationInMillis) {
		this.durationInMillis = durationInMillis;
	}

	public String getEndActivityId() {
		return endActivityId;
	}

	public void setEndActivityId(String endActivityId) {
		this.endActivityId = endActivityId;
	}

	public String getStartUserId() {
		return startUserId;
	}

	public void setStartUserId(String startUserId) {
		this.startUserId = startUserId;
	}

	public String getStartActivityId() {
		return startActivityId;
	}

	public void setStartActivityId(String startActivityId) {
		this.startActivityId = startActivityId;
	}

	public String getDeleteReason() {
		return deleteReason;
	}

	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}

	public String getSuperProcessInstanceId() {
		return superProcessInstanceId;
	}

	public void setSuperProcessInstanceId(String superProcessInstanceId) {
		this.superProcessInstanceId = superProcessInstanceId;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Map<String, Object> getProcessVariables() {
		return processVariables;
	}

	public void setProcessVariables(Map<String, Object> processVariables) {
		this.processVariables = processVariables;
	}

}
