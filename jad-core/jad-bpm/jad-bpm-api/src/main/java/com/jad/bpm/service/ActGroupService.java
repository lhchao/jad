package com.jad.bpm.service;

import java.util.List;

import com.jad.bpm.dto.ActGroup;

public interface ActGroupService {
	
	/**
	 * 跟据id查询用户所属组列表
	 * @param userId
	 * @return
	 */
	public List<ActGroup> findActGroupsByUser(String userId) ;

}
