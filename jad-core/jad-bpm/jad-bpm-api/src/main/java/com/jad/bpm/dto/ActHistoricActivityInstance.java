package com.jad.bpm.dto;

import java.io.Serializable;
import java.util.Date;

public class ActHistoricActivityInstance implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	/** The unique identifier of this historic activity instance. */
	private String id;

	/** The unique identifier of the activity in the process */
	private String activityId;

	/** The display name for the activity */
	private String activityName;

	/** The XML tag of the activity as in the process file */
	private String activityType;

	/** Process definition reference */
	private String processDefinitionId;

	/** Process instance reference */
	private String processInstanceId;

	/** Execution reference */
	private String executionId;

	/** The corresponding task in case of task activity */
	private String taskId;

	/** The called process instance in case of call activity */
	private String calledProcessInstanceId;

	/** Assignee in case of user task activity */
	private String assignee;

	/** Time when the activity instance started */
	private Date startTime;

	/** Time when the activity instance ended */
	private Date endTime;

	/** Difference between {@link #getEndTime()} and {@link #getStartTime()}. */
	private Long durationInMillis;

	/** Returns the tenant identifier for the historic activity */
	private String tenantId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getActivityId() {
		return activityId;
	}

	public void setActivityId(String activityId) {
		this.activityId = activityId;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getExecutionId() {
		return executionId;
	}

	public void setExecutionId(String executionId) {
		this.executionId = executionId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public String getCalledProcessInstanceId() {
		return calledProcessInstanceId;
	}

	public void setCalledProcessInstanceId(String calledProcessInstanceId) {
		this.calledProcessInstanceId = calledProcessInstanceId;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Long getDurationInMillis() {
		return durationInMillis;
	}

	public void setDurationInMillis(Long durationInMillis) {
		this.durationInMillis = durationInMillis;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	
}
