package com.jad.bpm.dto;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public class ActDelegateExecution implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Unique id of this path of execution that can be used as a handle to
	 * provide external signals back into the engine after wait states.
	 */
	private String id;

	/** Reference to the overall process instance */
	private String processInstanceId;

	/**
	 * The {@link ExecutionListener#EVENTNAME_START event name} in case this
	 * execution is passed in for an {@link ExecutionListener}
	 */
	private String eventName;

	/**
	 * The business key for this execution. Only returns a value if the delegate
	 * execution is a process instance.
	 * 
	 *  use {@link #getProcessBusinessKey()} to get the business key
	 *             for the process associated with this execution, regardless
	 *             whether or not this execution is a process-instance.
	 */
	private String businessKey;

	/**
	 * The business key for the process instance this execution is associated
	 * with.
	 */
	private String processBusinessKey;

	/**
	 * The process definition key for the process instance this execution is
	 * associated with.
	 */
	private String processDefinitionId;

	/**
	 * Gets the id of the parent of this execution. If null, the execution
	 * represents a process-instance.
	 */
	private String parentId;

	/**
	 * Gets the id of the current activity.
	 */
	private String currentActivityId;

	/**
	 * Gets the name of the current activity.
	 */
	private String currentActivityName;

	/**
	 * Returns the tenant id, if any is set before on the process definition or
	 * process instance.
	 */
	private String tenantId;

	private Map<String, Object> variables;

	private Map<String, Object> variablesLocal;

	private String variableName;

	private Set<String> variableNames;

	private Set<String> variableNamesLocal;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}

	public String getProcessBusinessKey() {
		return processBusinessKey;
	}

	public void setProcessBusinessKey(String processBusinessKey) {
		this.processBusinessKey = processBusinessKey;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getCurrentActivityId() {
		return currentActivityId;
	}

	public void setCurrentActivityId(String currentActivityId) {
		this.currentActivityId = currentActivityId;
	}

	public String getCurrentActivityName() {
		return currentActivityName;
	}

	public void setCurrentActivityName(String currentActivityName) {
		this.currentActivityName = currentActivityName;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public Map<String, Object> getVariables() {
		return variables;
	}

	public void setVariables(Map<String, Object> variables) {
		this.variables = variables;
	}

	public Map<String, Object> getVariablesLocal() {
		return variablesLocal;
	}

	public void setVariablesLocal(Map<String, Object> variablesLocal) {
		this.variablesLocal = variablesLocal;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

	public Set<String> getVariableNames() {
		return variableNames;
	}

	public void setVariableNames(Set<String> variableNames) {
		this.variableNames = variableNames;
	}

	public Set<String> getVariableNamesLocal() {
		return variableNamesLocal;
	}

	public void setVariableNamesLocal(Set<String> variableNamesLocal) {
		this.variableNamesLocal = variableNamesLocal;
	}

}
