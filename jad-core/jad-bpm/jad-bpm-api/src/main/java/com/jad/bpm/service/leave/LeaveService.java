/**
 * There are <a href="https://github.com/thinkgem/jeesite">JeeSite</a> code generation
 */
package com.jad.bpm.service.leave;

import java.util.List;
import java.util.Map;

import com.jad.bpm.dto.leave.Leave;
import com.jad.commons.service.BaseService;
import com.jad.commons.vo.Page;

/**
 * 请假Service
 * @author liuj
 * @version 2013-04-05
 */
public interface LeaveService extends BaseService {
	
	/**
	 * 获取流程详细及工作流参数
	 * @param id
	 */
	public Leave get(String id) ;
	
	/**
	 * 启动流程
	 * @param entity
	 */
	public void save(Leave leave, Map<String, Object> variables) ;

	/**
	 * 查询待办任务
	 * @param userId 用户ID
	 * @return
	 */
	public List<Leave> findTodoTasks(String userId) ;

	public Page<Leave> find(Page<Leave> page, Leave leave) ;
}
