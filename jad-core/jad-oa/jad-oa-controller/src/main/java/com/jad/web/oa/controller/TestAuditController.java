/**
 * Copyright &copy; 2012-2014 <a href="https://github.com/thinkgem/jeesite">JeeSite</a> All rights reserved.
 */
package com.jad.web.oa.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jad.web.mvc.BaseController;

/**
 * 审批Controller
 * @author thinkgem
 * @version 2014-05-16
 */
@Controller
@RequestMapping(value = "${adminPath}/oa/testAudit")
public class TestAuditController extends BaseController {

	protected Logger logger = LoggerFactory.getLogger(getClass());
	
//	@Autowired
//	private TestAuditService testAuditService;
	
//	@Autowired
//	protected SystemService systemService;
	
//	@ModelAttribute
//	public TestAuditVo get(@RequestParam(required=false) String id){//, 
////			@RequestParam(value="act.procInsId", required=false) String procInsId) {
//		TestAuditVo testAudit = null;
//		if (StringUtils.isNotBlank(id)){
//			testAudit = testAuditService.findById(id);
////		}else if (StringUtils.isNotBlank(procInsId)){
////			testAudit = testAuditService.getByProcInsId(procInsId);
//		}
//		if (testAudit == null){
//			testAudit = new TestAuditVo();
//		}
//		return testAudit;
//	}
//	
//	@RequiresPermissions("oa:testAudit:view")
//	@RequestMapping(value = {"list", ""})
//	public String list(TestAuditVo testAudit, HttpServletRequest request, HttpServletResponse response, Model model) {
////		UserVo user = systemService.getUser(super.getCurrentUserId());
////		if (!user.isAdmin()){
//////			testAudit.setCreateBy(user);
////		}
//        Page<TestAuditVo> page = testAuditService.findPage(getPage(request, response), testAudit); 
//        model.addAttribute("page", page);
//		return "modules/oa/testAuditList";
//	}
//	
//	/**
//	 * 申请单填写
//	 * @param testAudit
//	 * @param model
//	 * @return
//	 */
//	@RequiresPermissions("oa:testAudit:view")
//	@RequestMapping(value = "form")
//	public String form(TestAuditVo testAudit, Model model) {
//		
//		String view = "testAuditForm";
//		
//		// 查看审批申请单
////		if (StringUtils.isNotBlank(testAudit.getId())){//.getAct().getProcInsId())){
////
////			// 环节编号
////			String taskDefKey = testAudit.getAct().getTaskDefKey();
////			
////			// 查看工单
////			if(testAudit.getAct().isFinishTask()){
////				view = "testAuditView";
////			}
////			// 修改环节
////			else if ("modify".equals(taskDefKey)){
////				view = "testAuditForm";
////			}
////			// 审核环节
////			else if ("audit".equals(taskDefKey)){
////				view = "testAuditAudit";
//////				String formKey = "/oa/testAudit";
//////				return "redirect:" + ActUtils.getFormUrl(formKey, testAudit.getAct());
////			}
////			// 审核环节2
////			else if ("audit2".equals(taskDefKey)){
////				view = "testAuditAudit";
////			}
////			// 审核环节3
////			else if ("audit3".equals(taskDefKey)){
////				view = "testAuditAudit";
////			}
////			// 审核环节4
////			else if ("audit4".equals(taskDefKey)){
////				view = "testAuditAudit";
////			}
////			// 兑现环节
////			else if ("apply_end".equals(taskDefKey)){
////				view = "testAuditAudit";
////			}
////		}
//
//		model.addAttribute("testAudit", testAudit);
//		return "modules/oa/" + view;
//	}
//	
//	/**
//	 * 申请单保存/修改
//	 * @param testAudit
//	 * @param model
//	 * @param redirectAttributes
//	 * @return
//	 */
//	@RequiresPermissions("oa:testAudit:edit")
//	@RequestMapping(value = "save")
//	public String save(TestAuditVo testAudit, Model model, RedirectAttributes redirectAttributes) {
//		if (!beanValidator(model, testAudit)){
//			return form(testAudit, model);
//		}
////		if(StringUtils.isNotBlank(testAudit.getId())){
////			testAuditService.update(testAudit);
////		}else{
////			testAuditService.add(testAudit);
////		}
////		testAuditService.save(testAudit);
////		addMessage(redirectAttributes, "提交审批'" + testAudit.getUser().getName() + "'成功");
//		addMessage(redirectAttributes, "提交审批成功");
//		return "redirect:" + adminPath + "/act/task/todo/";
//	}
//
//	/**
//	 * 工单执行（完成任务）
//	 * @param testAudit
//	 * @param model
//	 * @return
//	 */
//	@RequiresPermissions("oa:testAudit:edit")
//	@RequestMapping(value = "saveAudit")
//	public String saveAudit(TestAuditVo testAudit, Model model) {
//		
////		if (StringUtils.isBlank(testAudit.getAct().getFlag())
////				|| StringUtils.isBlank(testAudit.getAct().getComment())){
////			addMessage(model, "请填写审核意见。");
////			return form(testAudit, model);
////		}
//		
//		testAuditService.auditSave(testAudit);
//		return "redirect:" + adminPath + "/act/task/todo/";
//	}
//	
//	/**
//	 * 删除工单
//	 * @param id
//	 * @param redirectAttributes
//	 * @return
//	 */
//	@RequiresPermissions("oa:testAudit:edit")
//	@RequestMapping(value = "delete")
//	public String delete(TestAuditVo testAudit, RedirectAttributes redirectAttributes) {
//		testAuditService.delete(testAudit);
//		addMessage(redirectAttributes, "删除审批成功");
//		return "redirect:" + adminPath + "/oa/testAudit/?repage";
//	}

}
