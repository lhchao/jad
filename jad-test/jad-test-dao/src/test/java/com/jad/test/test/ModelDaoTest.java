package com.jad.test.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.util.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.jad.commons.vo.Page;
import com.jad.commons.vo.PageQo;
import com.jad.test.dao.BaseDaoTestCase;
import com.jad.test.dao.ModelDao;
import com.jad.test.entity.ModelEo;
import com.jad.test.qo.ModelQo;

public class ModelDaoTest extends BaseDaoTestCase {
	
	private String newId="181b6120f8b64eed98cef01632fa953b";
	
	private int intType = 0;
	
	private ModelDao dao = null;
	
	
	
	
	@BeforeTest
	public void daoBeforeTest(){
		dao = super.getBean("modelDao");
	}
	
	@Test
	public void mainTest(){
		add();
//		findById();
//		updateById();
//		findByQo();
//		updateByIdList();
//		updateByQo();
//		deleteById();
//		deleteByIdList();
//		deleteByQo();
//		executeSql();
//		findMapByQo();
//		findMapBySql();
		
//		for(int i=0;i<=30;i++){
//			intType = i;
//			add();
//		}
		
//		findPageByQo();
		
		System.out.println("ok");
	}
	
//	@Test
	public void findById(){
		ModelEo eo=dao.findById("1");
		Assert.notNull(eo);
//		System.out.println(JsonMapper.toJsonString(eo));
		eo=dao.findById("2");
		Assert.notNull(eo);
//		System.out.println(JsonMapper.toJsonString(eo));
	}
	
	
//	@Test
	public void findPageByQo(){
		ModelQo qo = new ModelQo();
		
		PageQo pageqo = new PageQo(3,5);
		
		Page<ModelEo> eoPage = dao.findPageByQo(pageqo, qo);
		
		System.out.println(eoPage.getList().size());
		
		
	}
	
	
	
//	@Test
	public void add(){
		ModelEo eo = newEo();
		dao.add(eo);
	}
	
	

	

//	@Test
	public void updateById(){
		ModelEo eo=dao.findById(newId);
		int i=eo.getIntType();
		i++;
		ModelEo ueo=new ModelEo();
		ueo.setIntType(i);
		dao.updateById(ueo, eo.getId());
		eo=dao.findById(newId);
		Assert.isTrue(eo.getIntType().intValue()==i);
	}
	
//	@Test
	public void updateByQo(){
		ModelQo qo=new ModelQo();
		qo.setIntType(55);
		
		ModelEo eo=new ModelEo();
		eo.setIntType(44);
		
		dao.updateByQo(eo, qo);
		
		qo.setIntType(44);
		
		List<ModelEo> eoList = dao.findByQo(qo);
		Assert.isTrue(eoList.size()==2);
		
		
	}
	
//	@Test
	public void updateByIdList(){
		
		ModelEo eo=new ModelEo();
		eo.setIntType(55);
		
		List<String> idList = new ArrayList<String>();
		idList.add("af3bbf8f12034d438e48e0d314bdc35f");
		idList.add("fd0bd05465dd4d4981517c11d5f47266");
		
		dao.updateByIdList(eo, idList);
		
		ModelQo qo=new ModelQo();
		qo.setIntType(55);
		List<ModelEo> eoList = dao.findByQo(qo);
		Assert.isTrue(eoList.size()==2);
		
		
	}
	
//	@Test
	public void deleteById(){
		String id="c17b2e43a33542829821600767bed613";
		
		dao.deleteById(id);
		
		ModelEo eo=dao.findById(id);
		Assert.isNull(eo);
		
		
	}
	
//	@Test
	public void deleteByIdList(){
		
		List<String> idList = new ArrayList<String>();
		idList.add("63a50608e9c2496aba6b6bda2ee9343d");
		idList.add("febbd0044fe745f89fd657fa8016727f");
		
		dao.deleteByIdList(idList);
		
		ModelEo eo=dao.findById(idList.get(0));
		Assert.isNull(eo);
		eo=dao.findById(idList.get(1));
		Assert.isNull(eo);
	}
	
//	@Test
	public void deleteByQo(){
		String id="fd0bd05465dd4d4981517c11d5f47266";
		ModelQo qo=new ModelQo();
//		qo.setId(id);
		
		dao.deleteByQo(qo);
		
		ModelEo eo = dao.findById(id);
		
		Assert.isNull(eo);
		
		
	}
	

	
//	@Test
	public void findByQo(){
		ModelQo qo = new ModelQo();
		List<ModelEo> eoList = dao.findByQo(qo);
		System.out.println("记录数:"+eoList.size());
		
	}
	

	
//	@Test
	public void findBySql(){
	}
	
//	@Test
	public void findPageBySql(){
	}
	
//	@Test
	public void executeSql(){
		List params=new ArrayList();
		params.add(66);
		params.add("af3bbf8f12034d438e48e0d314bdc35f");
		
		String sql="update test_model m set m.int_type=? where id=? ";
		
		dao.executeSql(sql,params);
		
	}
	
//	@Test
	public void findMapByQo(){
		ModelQo qo=new ModelQo();
		qo.setIntType(66);
		
		List<Map<String,?>>mapList = dao.findMapByQo(qo, ModelEo.class);
		
		System.out.println(mapList.size());
	}
	
//	@Test
	public void findMapBySql(){
		String sql="select id from test_model ";
		
		List<Map<String,?>>mapList = dao.findMapBySql(sql, new ArrayList());
		
		System.out.println(mapList.size());
	}
	
//	@Test
	public void findPageMapByQo(){
		
	}
	
//	@Test
	public void findPageMapBySql(){
		
	}
	
	
	
	
	private ModelEo newEo(){
		
		ModelEo eo=new ModelEo();
		
//		newId=IdGen.uuid();
		
		eo.setId(newId);
		
		eo.setVarcharType("varcharType");
		
		eo.setIntType(intType);
		
		eo.setDoubleType(8.98);
		
//		eo.setBolbType(bolbType);
		
		eo.setTextType("textType");
		
		eo.setBooleanType(true);
		
		eo.setCreateBy("createBy");
		
		eo.setCreateDate(new Date());
		
		eo.setUpdateBy("updateBy");
		
		eo.setUpdateDate(new Date());
		
		eo.setRemarks("remarks");
		
		eo.setDelFlag("0");
		
		
		return eo;
	}
	
	

}
